/*******UPDATE********/
UPDATE comercios SET nombre_comercio = "NUEVO NOMBRE" 
WHERE nombre_comercio = "XXX"

/************PROCEDIMIENTO ALMACENADO INSERTAR************/
USE quejas;
DROP procedure IF EXISTS ingresar_queja;

DELIMITER $$
CREATE PROCEDURE ingresar_queja (_queja longtext, _fecha_documento DATE, _fecha_queja DATE, 
	_no_documento VARCHAR(30), _solicitud longtext,
	_nombre_comercio varchar(100), _nit varchar(10), _razon_social varchar(100),
	_direccion varchar(255), _telefono varchar(8), _correo varchar(100), _departamento_id BIGINT, _municipio_id BIGINT )
BEGIN
	set @cnt_comercio = (select count(id) from comercios where nit = _nit);
	
	if @cnt_comercio > 0 then    
	        
	    set @cnt_sucursal = (select count(id) from sucursales where direccion = _direccion AND comercio_id = @comercio_id AND municipio_id = _municipio_id);
	    
	    if @cnt_sucursal > 0 then
			set @sucursal_id = (select id from sucursales where direccion = _direccion AND comercio_id = @comercio_id AND municipio_id = _municipio_id );
	        
	        INSERT INTO quejas (queja, fecha_documento, fecha_queja, no_documento, solicitud, sucursal_id) 
			VALUES (_queja, _fecha_documento, _fecha_queja, _no_documento, _solicitud, @sucursal_id);
		else
	        set @comercio_id = (select id from comercios where nit = _nit);
	        
			INSERT INTO sucursales (direccion, telefono, correo, departamento_id, municipio_id, comercio_id) 
			VALUES (_direccion, _telefono, _correo, _departamento_id, _municipio_id, @comercio_id);
	        
	        set @sucursal_id = (select id from sucursales where direccion = _direccion AND comercio_id = @comercio_id AND municipio_id = _municipio_id );
	        
			INSERT INTO quejas (queja, fecha_documento, fecha_queja, no_documento, solicitud, sucursal_id) 
			VALUES (_queja, _fecha_documento, _fecha_queja, _no_documento, _solicitud, @sucursal_id);
	    end if;
	        
	else
		INSERT INTO comercios (nit, razon_social, nombre_comercio) 
		VALUES (_nit, _razon_social, _nombre_comercio);
	    
	    set @comercio_id = (select id from comercios where nit = _nit);
	    
	    INSERT INTO sucursales (direccion, telefono, correo, departamento_id, municipio_id, comercio_id) 
		VALUES (_direccion, _telefono, _correo, _departamento_id, _municipio_id, @comercio_id);
	    
	    set @sucursal_id = (select id from sucursales where direccion = _direccion AND comercio_id = @comercio_id AND municipio_id = _municipio_id );
	    
	    INSERT INTO quejas (queja, fecha_documento, fecha_queja, no_documento, solicitud, sucursal_id) 
		VALUES (_queja, _fecha_documento, _fecha_queja, _no_documento, _solicitud, @sucursal_id);
	end if;
END$$



CALL ingresar_queja('El comercio YYYY no me dio un buen servicio', '2021-09-03', "2021-09-03", 'DTE-123456', 'Quiero que me devuelvan el dinero.',
	'YYYY', '12345666', 'YYYY S.A', 'Barrio YYYY colonia X', '50203085', 'yyyy@gmail.com', 1, 1);


/*******Conteo de quejas de este año*********/

SELECT COUNT(Q.id) AS total_quejas FROM quejas Q 
INNER JOIN sucursales S ON S.id = Q.sucursal_id
INNER JOIN departamentos D ON D.id = S.departamento_id
INNER JOIN regiones R ON D.region_id = R.id
INNER JOIN comercios C ON C.id = S.comercio_id
    WHERE YEAR(Q.fecha_queja) = YEAR(CURDATE()) AND R.id  IN (2,6)
    AND C.id NOT IN (SELECT distinct C.id AS comercio_id FROM quejas Q 
        INNER JOIN sucursales S ON S.id = Q.sucursal_id
        INNER JOIN departamentos D ON D.id = S.departamento_id
        INNER JOIN regiones R ON D.region_id = R.id
        INNER JOIN comercios C ON C.id = S.comercio_id
            WHERE YEAR(Q.fecha_queja) = YEAR(CURDATE()) AND R.id  IN (3,4))



/**********Listado de quejas por fecha*************/

SELECT  Q.*, R.nombre AS Region,D.nombre AS Departamento, M.nombre AS Municipio  FROM quejas Q 
		  INNER JOIN sucursales S ON S.id = Q.sucursal_id
        INNER JOIN departamentos D ON D.id = S.departamento_id
        INNER JOIN regiones R ON R.id = D.region_id
        INNER JOIN municipios M ON S.municipio_id = M.id
        WHERE Q.deleted_at IS NULL AND Q.fecha_queja BETWEEN '2021-01-01' AND '2021-09-30'