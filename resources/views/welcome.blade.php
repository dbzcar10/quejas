@extends('layouts.inicio')
@section('contenido')



  <div class="row">
    <div class="col s12">
      @if (session()->has('mensaje'))
            <div class="alert alert-success">
                {{ session('mensaje') }}
            </div>
      @endif
    </div>


    @livewire('queja-form')

  </div>
@endsection

@push('js')
@endpush
