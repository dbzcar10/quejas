<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{--<title>Reporte 6A</title>--}}
    @yield('title')
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
{{--<link rel="stylesheet" href="{{asset('reportes/bootstrap.min.css')}}">--}}
<!-- Optional theme -->
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">--}}
<style>

  html{
    margin: 0;
  }
  .table {
      height: auto;
  }
  body{
    font-family: "Verdana" !important;
  }
  th {
    border: 1px solid #252850;
    color: #fff;
    font-weight: bold;
    background-color: #252850;
  }

  td {
      border: 1px solid black;
  }

</style>

@stack('styles')

</head>
<body>
    @yield('contenido')

</body>
</html>