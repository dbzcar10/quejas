<div class="sidebar" data-color="azure" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="{{route('dashboard')}}" class="simple-text logo-normal">
      <i class="material-icons">dashboard</i> {{ __('Dashboard') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('dashboard') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>

      {{--Usuarios--}}

      <li class="nav-item {{ ($activePage == 'users' || $activePage == 'users.edit') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#Users" aria-expanded="true">
          {{--<i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>--}}
          <i class="material-icons" style="color:blue">person</i>
          <p>{{ __('Usuarios') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse {{ ($activePage == 'users') ? ' show' : '' }}" id="Users">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'users' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('users') }}">
                <span class="sidebar-mini"> list </span>
                <span class="sidebar-normal">{{ __('Lista Usuarios') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>

      {{--Quejas--}}

      <li class="nav-item {{ ($activePage == 'quejas' || $activePage == 'quejas.show') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#Quejas" aria-expanded="true">
          {{--<i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>--}}
          <i class="material-icons" style="color:orange">feedback</i>
          <p>{{ __('Quejas') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse {{ ($activePage == 'quejas') ? ' show' : '' }}" id="Quejas">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'quejas' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('quejas') }}">
                <span class="sidebar-mini"> list </span>
                <span class="sidebar-normal">{{ __('Lista Quejas') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>

      {{--Comercios--}}

      <li class="nav-item {{ ($activePage == 'comercios' || $activePage == 'comercios.show' 
                           || $activePage == 'comercios.create' || $activePage == 'comercios.edit'
                           || $activePage == 'comercios.sucursales.create' ||  $activePage == 'comercios.sucursales.edit') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#Comercios" aria-expanded="true">
          {{--<i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>--}}
          <i class="material-icons" style="color:brown">business</i>
          <p>{{ __('Comercios') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse {{ ($activePage == 'comercios') ? ' show' : '' }}" id="Comercios">
          <ul class="nav">
            <li class="nav-item {{ $activePage == 'comercios' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('comercios') }}">
                <span class="sidebar-mini"> list </span>
                <span class="sidebar-normal">{{ __('Lista Comercios') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>

      {{--Reportes--}}

      <li class="nav-item {{ ($activePage == 'reportes' || $activePage == 'reportes.por_fecha') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#reportes" aria-expanded="true">
          <i class="material-icons" style="color:green">analytics</i>
          <p>{{ __('Reportes') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse {{ ($activePage == 'reportes' || $activePage == 'reportes.por_fecha') ? ' show' : '' }}" id="reportes">
          <ul class="nav">
            <li class="nav-item {{ $activePage == 'reportes' ? ' active' : '' }}">
              <a class="nav-link" href="{{route('reportes.general')}}" target="_blank">
                <span class="sidebar-mini"> RPT </span>
                <span class="sidebar-normal">{{ __('Reporte General') }} </span>
              </a>
            </li>
            <li class="nav-item {{ $activePage == 'reportes.por_fecha' ? ' active' : '' }}">
              <a class="nav-link" href="{{route('reportes.por_fecha')}}" >
                <span class="sidebar-mini"> RPT </span>
                <span class="sidebar-normal">{{ __('Reporte Por Fecha') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>

    </ul>
  </div>
</div>
