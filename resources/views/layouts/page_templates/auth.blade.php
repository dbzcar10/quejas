<div class="wrapper ">
  @include('layouts.navbars.sidebar')
  <div class="main-panel">
    @include('layouts.navbars.navs.auth')
    <div class="content">
      {{--<x-alert tipo="success" algo="algo">
        <x-slot name="titulo">
            Titulo 1
        </x-slot>
        <x-slot name="mensaje">
            Felicidades
        </x-slot>
      </x-alert>--}}
      @yield('content')
    </div>
    @include('layouts.footers.auth')
  </div>
</div>