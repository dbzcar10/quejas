<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('title')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/alpine.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/materialize.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
</head>

<body class="grey lighten-3">
    <nav>
        <div class="nav-wrapper blue darken-3">
            <a href="{{route('welcome')}}" class="brand-logo"> <img src=" {{asset('img/LogoDiaco.jpg')}}" alt="" width="228" style="padding:2px"> </a>
            <ul class="menu">
        
                <li class="hide-on-med-and-down" style="padding-left: 235px; font-size: 1.6em;">
                    DIACO
                </li>
            </ul>
            <a href="#" data-target="menu-principal" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                @guest
                    <li class="right"><a href="{{route('login')}}" class="btn waves-effect waves-light" style="background-color:#29A6D9"><i class="material-icons left"> login</i>Iniciar Sesión</a></li>
                @else
                    <li class="right"><a href="{{route('dashboard')}}" class="btn waves-effect waves-light" style="background-color:#29A6D9"><i class="material-icons left"> login</i>Ir a admin</a></li>
                @endguest
            </ul>
        </div>
    </nav>
    <ul class="sidenav" id="menu-principal">
        @guest
            <li class="right"><a href="{{route('login')}}" class="btn waves-effect waves-light" style="background-color:#29A6D9"><i class="material-icons left"> login</i>Iniciar Sesión</a></li>
        @else
            <li class="right"><a href="{{route('dashboard')}}" class="btn waves-effect waves-light" style="background-color:#29A6D9"><i class="material-icons left"> login</i>Ir a admin</a></li>
        @endguest
    </ul>
    <div id="app" class="principal">
        <div class="container">
            @yield('content')        
        </div>
    </div>

    <script>
        $('.sidenav').sidenav();
    </script>

    <!--  Plugin for Sweet Alert -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    @stack('js')

</body>    
</html>