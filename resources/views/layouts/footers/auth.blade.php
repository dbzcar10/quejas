<footer class="footer">
  <div class="container-fluid">
    {{--<nav class="float-left">
      <ul>
        <li>
          <a href="https://www.creative-tim.com/license">
              {{ __('Licenses') }}
          </a>
        </li>
      </ul>
    </nav>--}}
    <div class="copyright float-right">
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script>
      , Proyecto Seminario <i class="material-icons">computer</i> Carlos Benjamin Morales Orellana
    </div>
  </div>
</footer>