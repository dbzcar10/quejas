<form wire:submit.prevent="submit" style="padding: 1rem" id="QuejaForm">
    <div class="col s12">
        <h4 class="text-center">
            Registro de Quejas
        </h4>
        <div class="card">
            <div class="row black-text">
                <div class="card-content z-depth-2" style="background-color:#29A6D9; padding:0.6rem">
                    <h6 class="text-center white-text">Datos de Comercio</h6>
                </div>            
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="col s6">
                        <label for="nit">Nit</label>
                        <input type="text" wire:model.lazy="nit" class="validate {{$errors->has('nit') ? 'invalid': ''}}" id="nit" autofocus>
                        @error('nit') <span class="helper-text" data-error="{{ $message }}"></span> @enderror
                
                    </div>
            
                    <div class="col s6">
                        <label for="nombre_comercial">Nombre Comercial</label>
                        <input type="text" wire:model="nombre_comercial" class="validate {{$errors->has('nombre_comercial') ? 'invalid': ''}}">
                        @error('nombre_comercial') <span class="helper-text" data-error="{{ $message }}"></span> @enderror
                    </div>       
                </div> 
                
                <div class="row">
                    <div class="col s6">
                        <label for="razon_social">Razón Social</label>
                        <input type="text" wire:model="razon_social" class="validate {{$errors->has('razon_social') ? 'invalid': ''}}">
                        @error('razon_social') <span class="helper-text" data-error="{{ $message }}"></span> @enderror
                    </div>
            
                    <div class="col s6">
                        <label for="direccion">Dirección</label>
                        <input type="text" wire:model="direccion" class="validate {{$errors->has('direccion') ? 'invalid': ''}}">
                        @error('direccion') <span class="helper-text" data-error="{{ $message }}"></span> @enderror
                    </div>
                </div>  
            
                <div class="row">
                    <div class="col s6">
                        <label for="telefono">Teléfono</label>
                        <input type="number" wire:model="telefono" class="validate {{$errors->has('telefono') ? 'invalid': ''}}">
                        @error('telefono') <span class="helper-text" data-error="{{ $message }}"></span> @enderror
                
                    </div>
                    <div class="col s6">
                        <label for="correo">Email</label>
                        <input type="email" wire:model="correo" class="validate {{$errors->has('correo') ? 'invalid': ''}}">
                        @error('correo') <span class="helper-text" data-error="{{ $message }}"></span> @enderror
                    </div>
                    
                </div>  
            
                <div class="row">
                    <div class="col s6">
                        <label>Departamento:</label>
                        <select wire:model="departamento" id="departamento" class="browser-default" name="departamento">
                            <option value='' selected>Seleccione departamento</option>
                            @foreach($departamentos as $departamento)
                                <option value={{ $departamento->id }}>{{ $departamento->nombre }}</option>
                            @endforeach
                        </select>
                        @error('departamento') <span class="red-text">{{$message}}</span> @enderror
                    </div>
                    
                    <div class="col s6">
                        <label>Municipio:</label>
                        <select wire:model="municipio" id="municipio" class="browser-default" name="municipio">
                            <option value='' selected>Seleccione municipio</option>
                            @if(count($municipios) > 0)
                                @foreach($municipios as $municipio)
                                    <option value={{ $municipio->id }}>{{ $municipio->nombre }}</option>
                                @endforeach
                            @endif
                        </select>
                        @error('municipio') <span class="red-text">{{$message}}</span> @enderror
                    </div>
                </div>
                <br>  
            </div>            
        </div>

        <div class="card">
            <div class="row black-text">
                <div class="card-content z-depth-2" style="background-color:#29A6D9; padding:0.6rem">
                    <h6 class="text-center white-text">Datos de Queja</h6>
                </div>
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="col s6">
                        <label for="no_documento">No. Documento</label>
                        <input id="no_documento" wire:model="no_documento" class="validate">
                        @error('no_documento') <span class="red-text">{{$message}}</span> @enderror
                    </div>
                    <div class="col s6">
                        <label for="fecha_documento">Fecha Documento</label>
                        <input id="fecha_documento" wire:model="fecha_documento" class="validate" type="text">
                        {{--<x-input.date wire:model="fecha_documento" :error="$errors->first('fecha_documento')"/>--}}
                        @error('fecha_documento') <span class="red-text">{{$message}}</span> @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <label for="queja">Detalle de Queja</label>
                        <textarea id="queja" wire:model="queja" class="materialize-textarea" style="height: 15vh !important;"></textarea>
                        @error('queja') <span class="red-text">{{$message}}</span> @enderror
                    </div>
                </div>
            
                <div class="row">
                    <div class="col s12">
                        <label for="solicitud">Solicito que</label>
                        <textarea id="solicitud" wire:model="solicitud" class="materialize-textarea" style="height: 15vh !important;"></textarea>
                        @error('solicitud') <span class="red-text">{{$message}}</span> @enderror
                    </div>
                </div>
                
                <div class="row">
                    <div class="col s12">
                        <label for="documento">Subir documento</label>
                        <div class="file-field input-field" wire:ignore>
                            <div class="btn waves-effect waves-light" style="background-color:#29A6D9">
                              <span>Subir Documento</span>
                              <input type="file" wire:model="documento">
                            </div>
                            <div class="file-path-wrapper">
                              <input class="file-path validate" type="text" readonly>
                            </div>
                        </div>
                        @error('documento') <span class="red-text">{{$message}}</span> @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="card-action right-align">
            <button type="submit" class="btn waves-effect waves-light light-blue darken-3 col s4 offset-s8">Guardar</button>
        </div>
        <br><br><br>
    </div>
    
</form>


@push('js')

<script>
    /*const nit = document.getElementById('nit');
    nit.addEventListener('focusout', (event) => {
        console.log("salio del in"); 
    });*/

    var picker = new Pikaday({
        field: document.getElementById('fecha_documento'),
        format: 'DD-MM-YYYY',
        onSelect: function() {
            @this.set('fecha_documento', this.getMoment().format('DD-MM-YYYY'));
            //console.log(this.getMoment().format('DD MM YYYY'));
        }
    });

    document.addEventListener("DOMContentLoaded", () => {
        Livewire.on('message', message => {
            Swal.fire(
                'Exito!',
                message,
                'success'
            );
            $('.file-path').val('');
        });
        Livewire.on('error', message => {
            Swal.fire(
                'Alerta!',
                message,
                'error'
            )
        });

        /*$('#fecha_documento').on('change', function (e) {
            @this.set('fecha_documento', e.target.value);

            console.log("la fecha es: "+@this.fecha_documento);
        });*/
    });

</script>

@endpush