<div class="row">
    <label class="pl-3">Filtrar por:</label>
    <div class="col-md-3">
        <label>Región</label>
        <select data-column="3" class="filter-select" id="select-region" wire:model="region" style="width: 100%">
        <option value="">Todas las regiones</option>
            @foreach ($regiones as $region)
            <option value="{{$region->nombre}}">{{$region->nombre}}</option>
            @endforeach
        </select>
    </div>

    <div class="col-sm-3">
        <label>Departamento</label>
        <select data-column="4" class="filter-select" id="select-departamento" wire:model="departamento" style="width: 100%">
        <option value="">Todos los departamentos</option>
            @if(count($departamentos) > 0)
                @foreach ($departamentos as $departamento)
                <option value="{{$departamento->nombre}}">{{$departamento->nombre}}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-3">
        <label>Municipio</label>
        <select data-column="5" class="filter-select" id="select-municipio" wire:model="municipio" style="width: 100%">
        <option value="">Todos los municipios</option>
            @if(count($municipios) > 0)
                @foreach($municipios as $municipio)
                    <option value="{{ $municipio->nombre }}">{{ $municipio->nombre }}</option>
                @endforeach
            @endif
        </select>
    </div>

</div>
