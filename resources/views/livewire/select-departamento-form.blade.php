<div>
    <div class="row">
        <label class="col-sm-2 col-form-label">Departamento:</label>
        <div class="form-group col-sm-4">
            <select wire:model="departamento" id="departamento" class="form-control" name="departamento">
                <option value=''>Seleccione departamento</option>
                @foreach($departamentos as $departamento)
                    <option value="{{ $departamento->id }}">{{ $departamento->nombre }}</option>
                @endforeach
            </select>
            @error('departamento') <div class="error">{{$message}}</div> @enderror
        </div>
        
        <label class="col-sm-2 col-form-label">Municipio:</label>
        <div class="form-group col-sm-4">
            <select wire:model="municipio" id="municipio" class="form-control" name="municipio">
                <option value='' selected>Seleccione municipio</option>
                @if(count($municipios) > 0)
                    @foreach($municipios as $municipio)
                        <option value="{{ $municipio->id }}">{{ $municipio->nombre }}</option>
                    @endforeach
                @endif
            </select>
            @error('municipio') <div class="error">{{$message}}</div> @enderror
        </div>
    </div>
</div>
