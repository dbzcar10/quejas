<form wire:submit.prevent="submit">
    <input type="text" wire:model="name" class="form-control">
    @error('name') {{ $message }} @enderror

    <input type="text" wire:model="email" class="form-control">
    @error('email') {{ $message }} @enderror


    <div class="row">
        <div class="col m-8">
            <label>Departamento:</label>
            <select name="departamento" wire:model="departamento">
                <option value=''>Seleccione departamento</option>
                @foreach($departamentos as $departamento)
                    <option value={{ $departamento->id }}>{{ $departamento->nombre }}</option>
                @endforeach
            </select>
        </div>
        @if(count($municipios) > 0)
            <div class="col m-8">
                <label>Municipio:</label>
                <select name="municipio" wire:model="municipio">
                    <option value=''>Seleccione municipio</option>
                    @foreach($municipios as $municipio)
                        <option value={{ $municipio->id }}>{{ $municipio->nombre }}</option>
                    @endforeach
                </select>
    
            </div>
        @endif
    </div>

    <button type="submit" class="btn waves-effect waves-light">Guardar</button>
</form>


{{--<script>
    $('select').formSelect();
</script>--}}