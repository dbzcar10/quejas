@extends('layouts.login.login')
@section('title')
<title>Login</title>
@endsection
@section('content')
<form method="POST" action="{{ route('login') }}">
    @csrf
    <div id="login-page" class="row">
        <div class="col s12 z-depth-4 card-panel" style="padding-left: 2rem; padding-right:2rem">
            <div class="row" style="padding-bottom:2rem">
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix" autofocus>mail_outline</i>
                    <input class="validate {{$errors->has('email') ? 'invalid': ''}}" id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                    <label for="email">Email</label>
                    @error('email')
                        <span class="helper-text" data-error="{{ $message }}"></span>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix">lock_outline</i>
                    <input class="validate {{$errors->has('password') ? 'invalid': ''}}" id="password" type="password" name="password" required>
                    <label for="password">Password</label>
                    @error('password')
                        <span class="helper-text" data-error="{{ $message }}"></span>
                    @enderror
                </div>
            </div>
            <div class="row">          
                <div class="input-field col s12 m12 l12">
                    <label>
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span>Recuerdame</span>
                    </label>
                </div>            
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button type="submit" class="btn waves-effect waves-light col s12" style="background-color:#0277bd ;">Login</button>
                </div>
            </div>
            <div class="row">
                {{--<div class="input-field col s6 m6 l6">
                    <p class="margin medium-small"><a href="#">Registrate ahora!</a></p>
                </div>--}}

                @if (Route::has('password.request'))
                        <div class="input-field col s12 m12 l12">
                            <p class="margin right-align medium-small"><a href="{{ route('password.request') }}">Olvidaste la contraseña?</a></p>
                        </div>  
                @endif
                        
            </div>
        </div>
    </div>
</form>
@endsection
@push('js')
    <script>
        @if(Session::has('MensajeEstado'))
            Swal.fire(
                'Alerta!',
                '{{ session("MensajeEstado") }}',
                'warning'
            );
        @endif
    </script>
@endpush