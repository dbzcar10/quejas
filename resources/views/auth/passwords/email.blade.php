@extends('layouts.login.login')
@section('title')
<title>Recuperar Password</title>
@endsection
@section('content')
    <div class="row" id="reset-page">
            {{--@if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif--}}

            <div class="z-depth-4 card" style="padding-left: 2rem; padding-right:2rem">

                <div class="card-content">
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix" autofocus>mail_outline</i>
                                <input class="validate {{$errors->has('email') ? 'invalid': ''}}" id="email" type="email" name="email" value="{{ old('email') }}" required autofocus autocomplete="email">
                                <label for="email">Email</label>
                                @error('email')
                                    <span class="helper-text" data-error="{{ $message }}"></span>
                                @enderror
                            </div>
                        </div>
        
                        <div class="row">
                            <button type="submit" class="btn waves-effect waves-light" style="background-color:#0277bd ;">
                                {{ __('Enviar link para restablecer contraseña') }}
                            </button>
                        </div>
                    </form> 
                </div>

            </div>

    </div>
@endsection

@push('js')
    <script>
        @if(Session::has('status'))
            Swal.fire(
                'Exito!',
                '{{ session("status") }}',
                'success'
            );
        @endif
    </script>
@endpush
