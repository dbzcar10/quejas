@extends('layouts.login.login')
@section('title')
<title>Reset Password</title>
@endsection
@section('content')
    <div class="row" id="reset-page">

            <div class="z-depth-4 card" style="padding-left: 2rem; padding-right:2rem">

                <div class="card-content">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix" autofocus>mail_outline</i>
                                <input class="validate {{$errors->has('email') ? 'invalid': ''}}" id="email" type="email" name="email" value="{{ old('email') }}" required autofocus autocomplete="email">
                                <label for="email">Email</label>
                                @error('email')
                                    <span class="helper-text" data-error="{{ $message }}"></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix" autofocus>lock_outline</i>
                                <input class="validate {{$errors->has('password') ? 'invalid': ''}}" id="password" type="password" name="password" value="{{ old('password') }}" required autocomplete="new-password">
                                <label for="password">Password</label>
                                @error('password')
                                    <span class="helper-text" data-error="{{ $message }}"></span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix" autofocus>lock_outline</i>
                                <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
                                <label for="password">Confirme Password</label>
                            </div>
                        </div>

                        <div class="row">
                            <button type="submit" class="btn waves-effect waves-light input-field col s12 m12 l12" style="background-color:#0277bd ;">
                                {{ __('Reset Password') }}
                            </button>
                        </div>
                    </form>
                </div>

            </div>

    </div>
@endsection