@extends('layouts.inicio')
@section('contenido')
    <br><br><br>
    <div class="col s12 text-center">
        <h2 style="font-weight: bold">404!</h2>
        <h4>La página solicitada no se puede encontrar</h4>
        <a href="{{URL::previous()}}" class="btn waves-effect waves-light light-blue darken-3">Click aqui para regresar</a>
    </div>

@endsection

@push('js')
@endpush