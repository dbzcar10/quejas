

{{--<div class="alert alert-{{$tipo}} alert-dismissible fade show" role="alert">
    <strong>{{$titulo}}</strong> {{$mensaje}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
</div>--}}


<div {{$attributes->merge(['class' => "alert alert-$tipo alert-dismissible fade show" ])}} role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <i class="material-icons">close</i>
  </button>
  <span><b>{{$titulo}}</b> {{$mensaje}}</span>
</div>