@component('mail::message')
@component('mail::table')
Hola,
para reiniciar su contraseña haga click en el siguiente enlace.
@endcomponent
@component('mail::button', ['url' => url('password/reset', $token) , 'color' => 'primary'])
Reset Password
@endcomponent
Este enlace caducara en 60 minutos.


Saludos, Administrador de {{config('app.name')}}, <br>
@endcomponent
