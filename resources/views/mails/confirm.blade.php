


@component('mail::message')
@component('mail::table')

Hola {{$user->name}},
Has cambiado tu correo electrónico. Por favor verifica la nueva dirección usando el siguiente enlace:
@endcomponent

@component('mail::button', ['url' => route('verificar', $user->verification_token)])
Verificar Correo
@endcomponent


Administrador de {{config('app.name')}}, <br>
@endcomponent
