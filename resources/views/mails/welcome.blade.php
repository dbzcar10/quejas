


@component('mail::message')
@component('mail::table')

Hola {{$user->name}},
Debes verificar tu cuenta utilizando el siguiente enlace:
@endcomponent

@component('mail::button', ['url' => route('verificar', $user->verification_token)])
Verificar Cuenta
@endcomponent


Administrador de {{config('app.name')}}, <br>
@endcomponent
