<div class='text-center row'>
    <div class="col-sm">
        <a href='{{route('sucursales.edit', ["sucursal" => $id, "comercio" => $comercio_id])}}' class="btn btn-sm btn-primary" title="Editar" data-toggle="tooltip" data-placement="top">
            <i class="material-icons md-24">mode</i>
        </a>
    </div>
    <div class="col-sm">
        <a href='{{route('sucursales.destroy', $id)}}' class='btn btn-sm btn-danger delete-sucursal' title='Eliminar Sucursal' data-method='delete' data-toggle="tooltip" data-placement="top">
            <i class="material-icons md-24">delete</i>
        </a>
    </div>
</div>


