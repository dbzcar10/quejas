@extends('layouts.admin', ['activePage' => 'comercios.sucursales.create', 'titlePage' => __('Agregar Sucursal')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-md-12">
                <div class="card card-hidden mb-3">
                    <div class="card-header card-header-primary text-center">
                        <h4>Agregar Sucursal</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('sucursales.store')}}" id="SucursalForm">
                            {{csrf_field()}}
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="correo">Correo</label>
                                <div class="col-sm-4 form-group">
                                    <input type="text" name="correo" class="form-control" id="correo" autofocus required value="{{old('correo')}}">
                                    @error('correo') <div class="error">{{$message}}</div> @enderror
                                </div>
                                <label class="col-sm-2 col-form-label" for="telefono">Teléfono</label>
                                <div class="col-sm-4 form-group">
                                    <input type="number" name="telefono" class="form-control" value="{{old('telefono')}}">
                                    @error('telefono') <div class="error">{{$message}}</div> @enderror
                                </div>    
                            </div>                                                       
                            
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="direccion">Dirección</label>
                                <div class="col-sm-10 form-group">
                                    <input type="text" name="direccion" class="form-control" value="{{old('direccion')}}">
                                    @error('direccion') <div class="error">{{$message}}</div> @enderror
                                </div>
                            </div>

                            <input type="hidden" name="comercio_id" value="{{$comercio->id}}">
                        
                            @livewire('select-departamento-form')
            
                            <br>
                            <div class="text-right m-t-15">
                                <a href="{{route('comercios.show', $comercio)}}" class="btn btn-default">Regresar</a>
                                <button class="btn btn-primary" id="btnGuardar">Guardar</button>
                            </div>
            
                        </form>
                    </div>
                </div>
          </div>
      </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
        $('.loader').fadeOut(225);
    });

  </script>
@endpush