@extends('layouts.admin', ['activePage' => 'comercios.sucursales.edit', 'titlePage' => __('Editar Sucursal')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-md-12">
                <div class="card card-hidden mb-3">
                    <div class="card-header card-header-primary text-center">
                        <h4>Editar Sucursal</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('sucursales.update', $sucursal)}}" id="SucursalForm">
                            {{csrf_field()}} {{method_field('PUT')}}
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="correo">Email</label>
                                <div class="col-sm-4 form-group">
                                    <input type="text" name="correo" class="form-control" id="correo" autofocus required value="{{old('correo', $sucursal->correo)}}">
                                    @error('correo') <div class="error">{{$message}}</div> @enderror
                                </div>
                                <label class="col-sm-2 col-form-label" for="telefono">Teléfono</label>
                                <div class="col-sm-4 form-group">
                                    <input type="number" name="telefono" class="form-control" value="{{old('telefono', $sucursal->telefono)}}">
                                    @error('telefono') <div class="error">{{$message}}</div> @enderror
                                </div>    
                            </div>                                                       
                            
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="direccion">Dirección</label>
                                <div class="col-sm-10 form-group">
                                    <input type="text" name="direccion" class="form-control" value="{{old('direccion', $sucursal->direccion)}}">
                                    @error('direccion') <div class="error">{{$message}}</div> @enderror
                                </div>
                            </div>

                            <input type="hidden" name="comercio_id" value="{{$comercio->id}}">
                        
                            @livewire('select-departamento-form', ['departamento' =>$sucursal->departamento_id, 'municipio' => $sucursal->municipio_id])
            
                            <br>
                            <div class="text-right m-t-15">
                                <a href="{{route('comercios.show', $comercio)}}" class="btn btn-default">Regresar</a>
                                <button class="btn btn-primary" id="btnGuardar">Actualizar</button>
                            </div>
            
                        </form>
                    </div>
                </div>
          </div>
      </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
        $('.loader').fadeOut(225);
    });

  </script>
@endpush