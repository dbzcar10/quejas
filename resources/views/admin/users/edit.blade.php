@extends('layouts.admin', ['activePage' => 'users.edit', 'titlePage' => __('Editar Usuario')])

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-login card-hidden mb-3">
                    <div class="card-header card-header-primary text-center">
                        <h4>Editar Usuario</h4>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{route('users.update', $usuario)}}" id="UserForm">
                            {{csrf_field()}} {{ method_field('PUT') }}
                            @include('admin.users.editForm')
            
            
                            <div class="text-right m-t-15">
                                <a href="{{route('users')}}" class="btn btn-default">Regresar</a>
                                <button class="btn btn-primary" id="btnActualizar">Actualizar</button>
                            </div>
            
                            
                        </form>
            
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loader loader-bar is-active"></div>
@endsection

@push('js')

<script>
    $(document).ready(function() {
        $('.loader').fadeOut(225);
        console.log('hole');
    });
    var validator = $("#UserForm").validate({
            ignore: [],
            onkeyup:false,
            onclick: false,
            //onfocusout: false,
            rules: {
                email: {
                    email: true
                }

            },
            messages: {
                email: {
                    email: "Por favor, ingrese un email válido"
                }
            }
        });
    $("#btnActualizar").click(function(event) {
        event.preventDefault();
        if ($('#UserForm').valid()) {
            this.form.submit();
            this.disabled= true;
            $('#btnActualizar').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Cargando...');
            $('.loader').fadeIn(225);
        } else {
            validator.focusInvalid();
        }
    });

</script>

@endpush
