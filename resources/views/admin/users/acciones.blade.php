<div class='text-center row'>
    {{--@can('update', App\User::find($id))--}}
    <div class="col-sm">
        <a href='{{route('users.edit', $id)}}' class="btn btn-sm" style="background-color: green;" title="Editar Usuario" data-toggle="tooltip" data-placement="top">
            <i class="material-icons md-24">edit</i>
        </a>
    </div>
    {{--@endcan--}}
    @if(!$email_verified_at)
    <div class="col-sm">
        <a href='{{route('users.resend', $id)}}' class="btn btn-sm" style="background-color: blue;" title='Reenviar Correo Confirmación' data-toggle="tooltip" data-placement="top">
            <i class="material-icons md-24">send</i>
        </a>
    </div>
    @endif

    @if($activo)
        <div class="col-sm">
            <a href='{{route('users.desactivar', $id)}}' class='btn btn-sm desactivar-usuario' data-method='delete' style="background-color: red;" title='Desactivar Usuario' data-toggle="tooltip" data-placement="top">
                <i class="material-icons md-24">person_off</i>
            </a>
        </div>
    @else
        <div class="col-sm">
            <a href='{{route('users.activar', $id)}}' class='btn btn-sm btn-info activar-usuario' data-method='post' title='Activar Usuario' data-toggle="tooltip" data-placement="top">
                <i class="material-icons md-24">thumb_up</i>
            </a>
        </div>
    @endif
</div>



