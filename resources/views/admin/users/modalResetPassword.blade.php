<!-- Modal -->
<div class="modal fade" id="modalResetPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form method="POST" id="ResetPasswordForm">
       {{--}} {{ method_field('put') }}--}}

      <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cambiar Contraseña Usuario Actual</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <label for="password_anterior" class="col-sm-4 col-form-label">Contraseña Anterior:</label>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <input type="password" name="password_anterior" placeholder="Ingrese contraseña anterior" class="form-control" value="{{old('password_anterior')}}">
                            {!! $errors->first('password_anterior', '<div class="error">:message</div>') !!}       
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label for="password" class="col-sm-4 col-form-label">Contraseña Nueva:</label>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <input type="password" name="password" placeholder="Ingrese contraseña nueva" class="form-control">
                            {!! $errors->first('password', '<div class="error">:message</div>') !!}       
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="password_confirmation" class="col-sm-4 col-form-label">Repite la contraseña:</label>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <input type="password" name="password_confirmation" placeholder="Repite contraseña" class="form-control">
                            {!! $errors->first('password', '<div class="error">:message</div>') !!}       
                        </div>
                    </div>
                </div>

              <input type="hidden" name="_token" id="tokenReset" value="{{ csrf_token() }}">
              {{--<input type="hidden" id="resetId" name="id">--}}

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary" id="btnResetPassword" >Actualizar</button>
            </div>
          </div>
        </div>
    </form>
</div>

@push('js')
   <script>

    $(document).ready(function(){
        $('#modalResetPassword').on('hide.bs.modal', function(){
            $("#ResetPasswordForm").validate().resetForm();
            document.getElementById("ResetPasswordForm").reset();
            window.location.hash = '#';

        });
    });

        //RESET password usuario actual
        var validator = $("#ResetPasswordForm").validate({
            ignore: [],
            onkeyup:false,
            onclick: false,
            //onfocusout: false,
            rules: {
                password_anterior: {
                    required: true
                },
                password: {
                    required: true
                },
                password_confirmation: {
                    required: true
                }

            },
            messages: {
                password_anterior: {
                    required: "Por favor, ingrese contraseña anterior"
                },
                password: {
                    required: "Por favor, ingrese contraseña"
                },
                password_confirmation: {
                    required: "Por favor, confirme contraseña"
                }
            }
        });

        $("#btnResetPassword").click(function(event) {
            event.preventDefault();
            if ($('#ResetPasswordForm').valid()) {
                resetModal();
            } else {
                validator.focusInvalid();
            }
        });

        function resetModal(button) {
            var formData = $("#ResetPasswordForm").serialize();
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('#tokenReset').val()},
                url: "{{route('users.reset')}}",
                data: formData,
                dataType: "json",
                success: function(data) {
                    $('#modalResetPassword').modal('hide');
                    $.notify({
                        message: "La contraseña se cambio correctamente.",
                        icon_type: 'class',
                        icon: "check"
                    },{
                        type: "success",
                    });
                },
                error: function(errors) {
                    if(errors.responseText !=""){
                        var errors = JSON.parse(errors.responseText);

                        if (errors.password != null) {
                            for (i = 0; i < errors.password.length; i++) {
                                $("#ResetPasswordForm input[name='password_confirmation'] ").after("<label class='error' id='ErrorPassword'>"+errors.password[i]+"</br></label>");
                            }
                        }
                        else{
                            $("#ErrorPassword").remove();
                        }

                        if (errors.password_anterior != null) {
                            $("input[name='password_anterior'] ").after("<label class='error' id='ErrorPassword_anterior'>"+errors.password_anterior+"</label>");
                        }
                        else{
                            $("#ErrorPassword_anterior").remove();
                        }
                    }

                }

            });
        }

    </script>
@endpush
