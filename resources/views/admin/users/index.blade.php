@extends('layouts.admin', ['activePage' => 'users', 'titlePage' => __('Usuarios')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-md-12">
              <div class="card card-login card-hidden mb-3">
                  <div class="card-header card-header-primary text-center">
                      <h4>Usuarios</h4>
                  </div>

                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 text-right">
                        <a href="{{route('users.create')}}" class="btn btn-sm btn-success">Agregar Usuario</a>
                      </div>
                    </div>
                    <div class="table-responsive">
                      <table class="table" id="users-table">
                        <thead class=" text-primary">
                          <tr>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Verificado</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
    });

    let users_table = $('#users-table').DataTable({
        "ajax": "{{route('users.getJson')}}",
        "columns": [
          { "data": "name" },
          { "data": "email" },
          { "data": 'email_verified_at'},
          { "data": 'activo'},
          { "data": "btn", orderable: false, searchable: false },
        ],

    });

    //Desactivar usuario
    $(document).on('click', 'a.desactivar-usuario', function(e) {
      e.preventDefault();
      var $this = $(this);
      
      Swal.fire({
        title: 'Esta seguro?',
        text: "si se desactiva ya no podrá usar el usuario",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#DD6B55',
        confirmButtonText: 'Si, desactivar!'
      }).then((result) => {          
        if (result.isConfirmed) {
          axios.delete($this.attr('href') )
          .then(function (response) {
            if(response.data.data === 'ok'){
              users_table.ajax.reload(null, false);
              Swal.fire(
                'Desactivado!',
                'El usuario se desactivo correctamente',
                'success'
              )
            }else{
              window.location = "{{url('/')}}"
            }

          })
          .catch(function (error) {
            if(error.response){

              if (error.response.status == 403) {
                console.log('Error:  '+ error.response.status);
                Swal.fire(
                  'No autorizado!',
                  'Usuario no autorizado para realizar esta accíon',
                  'warning'
                )
              } else {
                Swal.fire(
                  'Error!',
                  'Ocurrio un error contacte al administrador!',
                  'error'
                )
              }

            }else{
                console.log(error.message);
                Swal.fire(
                  'Error!',
                  'Ocurrio un error contacte al administrador!',
                  'error'
                )
            }
                          
          });
        }
      });
    });

    //Activar usuario
    $(document).on('click', 'a.activar-usuario', function(e) {
      e.preventDefault();
      var $this = $(this);
      
          axios.post($this.attr('href') )
          .then(function (response) {
            users_table.ajax.reload(null, false);
            Swal.fire(
              'Activado!',
              'El usuario se activo correctamente',
              'success'
            )
          })
          .catch(function (error) {
            if(error.response){

              if (error.response.status == 403) {
                console.log('Error:  '+ error.response.status);
                Swal.fire(
                  'No autorizado!',
                  'Usuario no autorizado para realizar esta accíon',
                  'warning'
                )
              } else {
                Swal.fire(
                  'Error!',
                  'Ocurrio un error contacte al administrador!',
                  'error'
                )
              }

            }else{
                console.log(error.message);
                Swal.fire(
                  'Error!',
                  'Ocurrio un error contacte al administrador!',
                  'error'
                )
            }
                          
          });
    });
  </script>
@endpush