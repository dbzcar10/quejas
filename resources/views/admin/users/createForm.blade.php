

<div class="row">
    <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
    <div class="col-sm-7">
        <div class="form-group">
            <input type="text" name="name" placeholder="Ingrese Nombre" class="form-control" value="{{old('name')}}">
            {!! $errors->first('name', '<div class="error">:message</div>') !!}       
        </div>
    </div>
</div>

<div class="row">
    <label for="email" class="col-sm-2 col-form-label">Email:</label>
    <div class="col-sm-7">
        <div class="form-group">
            <input type="email" name="email" placeholder="Ingrese Correo" class="form-control" value="{{old('email')}}">
            {!! $errors->first('email', '<div class="error">:message</div>') !!}
        </div>
    </div>
</div>

<div class="row">
    <label for="password" class="col-sm-2 col-form-label">Contraseña</label>
    <div class="col-sm-7">
        <div class="form-group">
            <input type="password" name="password" placeholder="Ingrese contraseña" class="form-control">
            {!! $errors->first('password', '<div class="error">:message</div>') !!}       
        </div>
    </div>
</div>
<div class="row">
    <label for="password_confirmation" class="col-sm-2 col-form-label">Repite la contraseña:</label>
    <div class="col-sm-7">
        <div class="form-group">
            <input type="password" name="password_confirmation" placeholder="Repite contraseña" class="form-control">
            {!! $errors->first('password', '<div class="error">:message</div>') !!}       
        </div>
    </div>
</div>

<br>


