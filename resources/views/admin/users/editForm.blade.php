

<div class="row">
    <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
    <div class="col-sm-7">
        <div class="form-group">
            <input type="text" name="name" placeholder="Ingrese Nombre" class="form-control" value="{{old('name', $usuario->name)}}">
            {!! $errors->first('name', '<div class="error">:message</div>') !!}       
        </div>
    </div>
</div>

<div class="row">
    <label for="email" class="col-sm-2 col-form-label">Email:</label>
    <div class="col-sm-7">
        <div class="form-group">
            <input type="email" name="email" placeholder="Ingrese Correo" class="form-control" value="{{old('email', $usuario->email)}}">
            {!! $errors->first('email', '<div class="error">:message</div>') !!}
        </div>
    </div>
</div>

<br>


