@extends('layouts.admin', ['activePage' => 'reportes.por_fecha', 'titlePage' => __('Reporte por Fechas')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-md-12">
                <div class="card card-hidden mb-3">
                    <div class="card-header card-header-primary text-center">
                        <h4>Reporte por rango de fecha</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('reportes.por_fecha_grafica')}}" id="ReportePorFechaForm">
                            {{csrf_field()}}  
                            
                            <div class="row">
                                <label for="fecha_inicio" class="col-sm-2 col-form-label">Fecha de Inicio</label>
                                <div class="col-sm-7">
                                    <div class="input-group mb-3">
                                        <i class="material-icons prefix">calendar_today</i> <input type="text" name="fecha_inicio" placeholder="Fecha de Inicio" class="form-control ml-2" id="fecha_inicio">
                                        {!! $errors->first('fecha_inicio', '<div class="error">:message</div>') !!}       
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label for="fecha_fin" class="col-sm-2 col-form-label">Fecha de Fin</label>
                                <div class="col-sm-7">
                                    <div class="input-group mb-3">
                                        <i class="material-icons prefix">calendar_today</i> <input type="text" name="fecha_fin" placeholder="Fecha de Fin" class="form-control ml-2" id="fecha_fin">
                                        {!! $errors->first('fecha_fin', '<div class="error">:message</div>') !!}       
                                    </div>
                                </div>
                            </div>
                            
                            <br>
                            <div class="text-right m-t-15">
                                <a href="{{route('dashboard')}}" class="btn btn-default">Regresar</a>
                                <button class="btn btn-primary" id="btnGenerar"><i class="material-icons">picture_as_pdf</i> Generar Reporte</button>
                            </div>
            
                        </form>
                    </div>
                </div>
          </div>
      </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
        $('.loader').fadeOut(225);
    });

    var validator = $("#ReportePorFechaForm").validate({
        ignore: [],
        onkeyup:false,
        onclick: false,
        //onfocusout: false,
        rules: {
            fecha_inicio:{
                    required: true,
            },
            fecha_fin:{
                    required: true,
            },
        },
        messages: {
            fecha_inicio: {
                required: "Por favor, seleccione fecha de inicio"
        },
            fecha_fin: {
                required: "Por favor, seleccione fecha de fin"
            },
        }
    });

    var fecha_inicio = new Pikaday({
        field: document.getElementById('fecha_inicio'),
        format: 'DD-MM-YYYY',
    });

    var fecha_fin = new Pikaday({
        field: document.getElementById('fecha_fin'),
        format: 'DD-MM-YYYY',
    });

    $("#btnGenerar").click(function(event) {
        event.preventDefault();
        if ($('#ReportePorFechaForm').valid()) {
            this.form.submit();
            this.disabled= true;
            $('#btnGenerar').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Cargando...');
            $('.loader').fadeIn(225);
        } else {
            validator.focusInvalid();
        }
    });

  </script>
@endpush