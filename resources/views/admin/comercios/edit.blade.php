@extends('layouts.admin', ['activePage' => 'comercios.edit', 'titlePage' => __('Editar Comercio')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-md-12">
                <div class="card card-hidden mb-3">
                    <div class="card-header card-header-primary text-center">
                        <h4>Editar Comercio</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('comercios.update', $comercio)}}" id="ComercioUpdateForm">
                            {{csrf_field()}} {{ method_field('PUT') }}
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="nit">Nit</label>
                                <div class="col-sm-4 form-group">
                                    <input type="text" name="nit" class="form-control" id="nit" autofocus required value="{{old('nit', $comercio->nit)}}">
                                    @error('nit') <div class="error">{{$message}}</div> @enderror
                                </div>
                                <label class="col-sm-2 col-form-label" for="nombre_comercial">Nombre Comercial</label>
                                <div class="col-sm-4 form-group">
                                    <input type="text" name="nombre_comercial" class="form-control" value="{{old('nombre_comercial', $comercio->nombre_comercio)}}">
                                    @error('nombre_comercial') <div class="error">{{$message}}</div> @enderror
                                </div>    
                            </div>                                                       
                            
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="razon_social">Razón Social</label>
                                <div class="col-sm-4 form-group">
                                    <input type="text" name="razon_social" class="form-control" value="{{old('razon_social', $comercio->razon_social)}}">
                                    @error('razon_social') <div class="error">{{$message}}</div> @enderror
                                </div>

                            </div>
            
                            <br>
                            <div class="text-right m-t-15">
                                <a href="{{route('comercios')}}" class="btn btn-default">Regresar</a>
                                <button class="btn btn-primary" id="btnActualizar">Actualizar</button>
                            </div>
            
                        </form>
                    </div>
                </div>
          </div>
      </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
        $('.loader').fadeOut(225);
    });

  </script>
@endpush