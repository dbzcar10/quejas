@extends('layouts.admin', ['activePage' => 'comercios.create', 'titlePage' => __('Agregar Comercio')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-md-12">
                <div class="card card-hidden mb-3">
                    <div class="card-header card-header-primary text-center">
                        <h4>Agregar Comercio</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{route('comercios.store')}}" id="ComercioForm">
                            {{csrf_field()}}
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="nit">Nit</label>
                                <div class="col-sm-4 form-group">
                                    <input type="text" name="nit" class="form-control" id="nit" autofocus required value="{{old('nit')}}">
                                    @error('nit') <div class="error">{{$message}}</div> @enderror
                                </div>
                                <label class="col-sm-2 col-form-label" for="nombre_comercial">Nombre Comercial</label>
                                <div class="col-sm-4 form-group">
                                    <input type="text" name="nombre_comercial" class="form-control" value="{{old('nombre_comercial')}}">
                                    @error('nombre_comercial') <div class="error">{{$message}}</div> @enderror
                                </div>    
                            </div>                                                       
                            
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="razon_social">Razón Social</label>
                                <div class="col-sm-4 form-group">
                                    <input type="text" name="razon_social" class="form-control" value="{{old('razon_social')}}">
                                    @error('razon_social') <div class="error">{{$message}}</div> @enderror
                                </div>
                                <label class="col-sm-2 col-form-label" for="direccion">Dirección</label>
                                <div class="col-sm-4 form-group">
                                    <input type="text" name="direccion" class="form-control" value="{{old('direccion')}}">
                                    @error('direccion') <div class="error">{{$message}}</div> @enderror
                                </div>
                            </div>
                        
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="telefono">Teléfono</label>
                                <div class="col-sm-4 form-group">
                                    <input type="number" name="telefono" class="form-control" value="{{old('telefono')}}">
                                    @error('telefono') <div class="error">{{$message}}</div> @enderror
                                </div>
                                <label class="col-sm-2 col-form-label" for="correo">Email</label>
                                <div class="col-sm-4 form-group">
                                    <input type="email" name="correo" class="form-control" value="{{old('correo')}}">
                                    @error('correo') <div class="error">{{$message}}</div> @enderror
                                </div>
                            </div>                            
                        
                            @livewire('select-departamento-form')
            
                            <br>
                            <div class="text-right m-t-15">
                                <a href="{{route('comercios')}}" class="btn btn-default">Regresar</a>
                                <button class="btn btn-primary" id="btnActualizar">Guardar</button>
                            </div>
            
                        </form>
                    </div>
                </div>
          </div>
      </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
        $('.loader').fadeOut(225);
    });

  </script>
@endpush