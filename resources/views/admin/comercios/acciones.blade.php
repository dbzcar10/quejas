<div class='text-center row'>
    {{--@can('update', App\User::find($id))--}}
    <div class="col-sm">
        <a href='{{route('comercios.show', $id)}}' class="btn btn-sm btn-info" title="Detalles" data-toggle="tooltip" data-placement="top">
            <i class="material-icons md-24">list</i>
        </a>
    </div>
    {{--@endcan--}}

    <div class="col-sm">
        <a href='{{route('comercios.edit', $id)}}' class="btn btn-sm btn-primary" title="Editar" data-toggle="tooltip" data-placement="top">
            <i class="material-icons md-24">mode</i>
        </a>
    </div>

    <div class="col-sm">
        <a href='{{route('comercios.destroy', $id)}}' class='btn btn-sm btn-danger delete-comercio' title='Eliminar Comercio' data-method='delete' data-toggle="tooltip" data-placement="top">
            <i class="material-icons md-24">delete</i>
        </a>
    </div>
</div>


