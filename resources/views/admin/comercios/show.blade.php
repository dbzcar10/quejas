@extends('layouts.admin', ['activePage' => 'comercios.show', 'titlePage' => __('Detalles de Comercio')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-md-12">
              <div class="card card-hidden mb-3">
                  <div class="card-header card-header-primary text-center">
                      <h4>Datos Comercio</h4>
                  </div>
                  <div class="card-body">
                    <div class="row">
                        <label class="col-sm-2 col-form-label" for="nit">Nit</label>
                        <div class="col-sm-4 form-group">
                            <input type="text" class="form-control" value="{{$comercio->nit}}" readonly>
                        </div>
                        <label class="col-sm-2 col-form-label" for="nombre_comercial">Nombre Comercial</label>
                        <div class="col-sm-4 form-group">
                            <input type="text" class="form-control" value="{{$comercio->nombre_comercio}}" readonly>
                        </div>    
                    </div>                                                       
                  
                    <div class="row">
                        <label class="col-sm-2 col-form-label" for="razon_social">Razón Social</label>
                        <div class="col-sm-4 form-group">
                            <input type="text"class="form-control" value="{{$comercio->razon_social}}" readonly>
                        </div>
                    </div>
                  </div>
              </div>

              <div class="card card-hidden mb-3">
                <div class="card-header card-header-info text-center">
                    <h4>Sucursales</h4>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-12 text-right">
                      <a href="{{route('sucursales.create', $comercio)}}" class="btn btn-sm btn-success">Agregar Sucursal</a>
                    </div>
                  </div>                                                     
                  <div class="table-responsive">
                    <table class="table" id="sucursales-table">
                      <thead class=" text-primary">
                        <tr>
                          <th>Dirección</th>
                          <th>Teléfono</th>
                          <th>Correo Electrónico</th>
                          <th>Departamento</th>
                          <th>Municipio</th>
                          <th># quejas</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
              <div class="text-right m-t-15">
                <a href="{{route('comercios')}}" class="btn btn-default">Regresar</a>
              </div>
          </div>
      </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
    });

    var sucursales_table = $('#sucursales-table').DataTable({
          ajax: {
            url: '{{route("sucursales.getJsonComercio", $comercio)}}',
          },
          "columns": [
            { "data": "direccion"},
            { "data": "telefono"},
            { "data": "correo"},
            { "data": "nombre_departamento"},
            { "data": "nombre_municipio" },
            { "data": "cantidad_quejas" },
            { "data": "btn", orderable: false, searchable: false },
          ],

    });

    $(document).on('click', 'a.delete-sucursal', function(e) {
      e.preventDefault();
      var $this = $(this);
      
      Swal.fire({
        title: 'Esta seguro?',
        text: "Si se elimina no se podrá recuperar",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#DD6B55',
        confirmButtonText: 'Si, eliminar!'
      }).then((result) => {     
        console.log(result);     
        if (result.isConfirmed) {
          axios.delete($this.attr('href') )
          .then(function (response) {
            console.log(response);
            sucursales_table.ajax.reload(null, false);
            Swal.fire(
              'Eliminado!',
              'La sucursal se elimino correctamente',
              'success'
            )
          })
          .catch(function (error) {
            console.log(error);
            if(error.response){

              if (error.response.status == 403) {
                console.log('Error:  '+ error.response.status);
                Swal.fire(
                  'No autorizado!',
                  'Usuario no autorizado para realizar esta accíon',
                  'warning'
                )
              } else {
                Swal.fire(
                  'Error!',
                  'Ocurrio un error contacte al administrador!',
                  'error'
                )
              }

            }else{
                console.log(error.message);
                Swal.fire(
                  'Error!',
                  'Ocurrio un error contacte al administrador!',
                  'error'
                )
            }
                          
          });
        }
      });


    });

  </script>
@endpush