@extends('layouts.admin', ['activePage' => 'comercios', 'titlePage' => __('Comercios')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="card card-hidden mb-3">
              <div class="card-header card-header-primary text-center">
                  <h4>Comercios</h4>
              </div>

              <div class="card-body">
                <div class="row">
                  <div class="col-12 text-right">
                    <a href="{{route('comercios.create')}}" class="btn btn-sm btn-success">Agregar Comercio</a>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table" id="comercios-table">
                    <thead class=" text-primary">
                      <tr>
                        <th>NIT</th>
                        <th>Nombre Comercio</th>
                        <th>Cantidad Sucursales</th>
                        <th>Cantidad Quejas</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                  </table>
                </div>
              </div>
          </div>
      </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
      //cargar_tabla();
    });
    var comercios_table = $('#comercios-table').DataTable({
          ajax: {
            url: '{{route("comercios.getJson")}}',
          },
          "columns": [
            { "data": "nit"},
            { "data": "nombre_comercio"},
            { "data": "cantidad_sucursales" },
            { "data": "cantidad_quejas" },
            { "data": "btn", orderable: false, searchable: false },
          ],

    });

    $(document).on('click', 'a.delete-comercio', function(e) {
      e.preventDefault();
      var $this = $(this);
      
      Swal.fire({
        title: 'Esta seguro?',
        text: "Si se elimina no se podrá recuperar",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#DD6B55',
        confirmButtonText: 'Si, eliminar!'
      }).then((result) => {          
        if (result.isConfirmed) {
          axios.delete($this.attr('href') )
          .then(function (response) {
            comercios_table.ajax.reload(null, false);
            Swal.fire(
              'Eliminado!',
              'El comercio se elimino correctamente',
              'success'
            )
          })
          .catch(function (error) {
            if(error.response){

              if (error.response.status == 403) {
                console.log('Error:  '+ error.response.status);
                Swal.fire(
                  'No autorizado!',
                  'Usuario no autorizado para realizar esta accíon',
                  'warning'
                )
              } else {
                Swal.fire(
                  'Error!',
                  'Ocurrio un error contacte al administrador!',
                  'error'
                )
              }

            }else{
                console.log(error.message);
                Swal.fire(
                  'Error!',
                  'Ocurrio un error contacte al administrador!',
                  'error'
                )
            }
                          
          });
        }
      });


    });
        
    
  </script>
@endpush