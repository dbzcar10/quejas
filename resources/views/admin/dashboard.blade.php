@extends('layouts.admin', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])
@push('styles')
<style>
  #chartdiv{
    width: 100%;
    height: 400px;
    margin: 1em 0
  }
  #legenddiv{
    min-height: 22rem;
  }
</style>
@endpush
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                <i class="material-icons">info_outline</i>
              </div>
              <p class="card-category">Total de Quejas</p>
              <h3 class="card-title">{{$quejas->count()}}</h3>
            </div>
            <div class="card-footer">
              {{--<div class="stats">
                <i class="material-icons">local_offer</i> Tracked from Github
              </div>--}}
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-primary card-header-icon">
              <div class="card-icon">
                <i class="material-icons">report</i>
              </div>
              <p class="card-category">Quejas del día</p>
              <h3 class="card-title">{{$quejasHoy[0]->total}}</h3>
            </div>
            <div class="card-footer">
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="material-icons">business</i>
              </div>
              <p class="card-category">Total de Comercios</p>
              <h3 class="card-title">{{$comercios->count()}}</h3>
            </div>
            <div class="card-footer">
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon">
                <i class="material-icons">work</i>
              </div>
              <p class="card-category">Total de Sucursales</p>
              <h3 class="card-title">{{$sucursales->count()}}</h3>
            </div>
            <div class="card-footer">
            </div>
          </div>
        </div>
      </div>
      <div class="row">
      <div class="card">
        <div class="card-header card-header-info">
          <h4>Quejas por Región</h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div id="chartdiv"></div>
            </div>
            <div class="col-md-6">
              <div id="legenddiv"></div>
            </div>
          </div>
        </div>
      </div>

      </div>
      {{-- ********GRÁFICA LINEAL******* --}}
      <div class="row">
        <div class="col-md-12">
          <div class="card card-chart">
            <div class="card-header card-header-success">
              <canvas id="graficaLineal" style="min-height: 180px; height: 180px; max-height: 180px; max-width: 100%;"></canvas>
            </div>
            <div class="card-body">
              <h4 class="card-title">Quejas en el año {{now()->year}}</h4>
              <p class="card-category">
                {{--<span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>--}}
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">access_time</i> última actualización <span class="pl-2" id="ultimaQueja"></span>
              </div>
            </div>
          </div>
        </div>
      </div>

      
    </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/maps.js"></script>
<script src="https://cdn.amcharts.com/lib/4/geodata/worldLow.js"></script>
<script src="https://cdn.amcharts.com/lib/4/geodata/guatemalaLow.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

  <script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
    });

  /*************GRAFICA LINEAL******************************/
    var labelMeses = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
    var quejasLineal = [];

    var ctx = document.getElementById('graficaLineal').getContext('2d');
    var dataChart = {
        labels: labelMeses,
        datasets: [{
            label: 'Número de Quejas',
            //color: 'white',
            backgroundColor     : 'white',//color de punto
            radius              : 4, //tamaño del punto
            pointStyle          : 'circle',
            hitRadius           : 7,
            hoverRadius         : 7,
            hoverBorderWidth    : 7,
            data: quejasLineal,
            borderColor: 'white',
            borderWidth: 3 //Grosor linea
        }]
    };


    const optionsChart = {
      maintainAspectRatio : false,
      plugins: {
        legend: {
          labels: {
            color: 'rgba(227, 205, 205,1)',
          }
        },
      },
      scales: {
          y: {
            beginAtZero: true,
            grid: {
              display: true,
              color: 'rgba(227, 205, 205, 0.6)',
              borderDash: [2, 2],
            },
            ticks:{
              color: 'rgba(227, 205, 205,1)',
            }
          },
          x: {
              grid: {
                display: true,
                color: 'rgba(227, 205, 205, 0.6)',
                borderDash: [2, 2],
              },
              ticks:{
                color: 'rgba(227, 205, 205,1)',
              }
          }
      },
      responsive              : true,
      datasetFill             : false
    };

    $.ajax({
        method: 'GET',
        url: '{{route('graficaLineal')}}',
        dataType: 'json',
        success: function (resp) {    
            $('#ultimaQueja').text(resp.data.ultimaQueja);  
            //asignamos los datos
            for(i=1;i<=12;i++){
              enteroQuejas = parseInt(resp.data.quejas[i]);
              quejasLineal.push(enteroQuejas);
            }
          //Inicializa la gráfica
          var barChartData = $.extend(true, {}, dataChart);
                  
          var graficaLineal = new Chart(ctx, {
              type: 'line',
              data: barChartData,
              options: optionsChart
          });
            
        },
        error: function (err) {
          console.warn(err);
          alert('Ocurrio un error, contacte a el Administrador');
        }
    })

    //**********Datos Mapa ********//
    var groupData = [
    {
      "name": "Región Metropolitana",
      "totalQuejas": {{DB::select($query, [1])[0]->total}}, 
      "color": '#B8860B',
      "data": [
        {
          "title": "Guatemala",
          "id": "GT-GU", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }
      ]
    },
    {
      "name": "Región Norte",
      "totalQuejas": {{DB::select($query, [2])[0]->total}},
      "color": 'purple',
      "data": [
        {
          "title": "Baja Verapaz",
          "id": "GT-BV", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }, 
      {
          "title": "Alta Verapaz",
          "id": "GT-AV",
          "customData": "1973"
        }
      ]
    },
    {
      "name": "Región Nororiental",
      "totalQuejas": {{DB::select($query, [3])[0]->total}},
      "color": 'blue',
      "data": [
        {
          "title": "El Progreso",
          "id": "GT-PR", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }, 
      {
          "title": "Izabal",
          "id": "GT-IZ",
          "customData": "1973"
        },
      {
          "title": "Zacapa",
          "id": "GT-ZA",
          "customData": "1973"
        },
      {
          "title": "Chiquimula",
          "id": "GT-CQ",
          "customData": "1973"
        }
      ]
    },
    {
      "name": "Región Suroriental",
      "totalQuejas": {{DB::select($query, [4])[0]->total}},
      "color": 'green',
      "data": [
        {
          "title": "Santa Rosa",
          "id": "GT-SR", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }, 
      {
          "title": "Jalapa",
          "id": "GT-JA",
          "customData": "1973"
        },
      {
          "title": "Jutiapa",
          "id": "GT-JU",
          "customData": "1973"
        }

      ]
    },
    {
      "name": "Región Central",
      "totalQuejas": {{DB::select($query, [5])[0]->total}},
      "color": 'orange',
      "data": [
        {
          "title": "Sacatepéquez",
          "id": "GT-SA", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }, 
      {
          "title": "Chimaltenango",
          "id": "GT-CM",
          "customData": "1973"
        },
      {
          "title": "Escuintla",
          "id": "GT-ES",
          "customData": "1973"
        }
      ]
    },
    {
      "name": "Región Suroccidental",
      "totalQuejas": {{DB::select($query, [6])[0]->total}},
      "color": 'indigo',
      "data": [
        {
          "title": "Sololá",
          "id": "GT-SO", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }, 
      {
          "title": "Totonicapán",
          "id": "GT-TO",
          "customData": "1973"
        },
      {
          "title": "Quetzaltenango",
          "id": "GT-QZ", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }, 
      {
          "title": "Suchitepéquez",
          "id": "GT-SU",
          "customData": "1973"
        },
      {
          "title": "Retalhuleu ",
          "id": "GT-RE", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }, 
      {
          "title": "San Marcos",
          "id": "GT-SM",
          "customData": "1973"
        }
      ]
    },
    {
      "name": "Región Noroccidental",
      "totalQuejas": {{DB::select($query, [7])[0]->total}},
      "color": '#87CEFA',
      "data": [
        {
          "title": "Huehuetenango ",
          "id": "GT-HU", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }, 
      {
          "title": "Quiché",
          "id": "GT-QC",
          "customData": "1973"
        }
      ]
    },
    {
      "name": "Región Péten",
      "totalQuejas": {{DB::select($query, [8])[0]->total}},
      "color": 'salmon',
      "data": [
        {
          "title": "Petén",
          "id": "GT-PE", // With MapPolygonSeries.useGeodata = true, it will try and match this id, then apply the other properties as custom data
          "customData": "1973"
        }
      ]
    }
];

  </script>
  <script src="{{asset('js/dashboard/mapas.js')}}"></script>
@endpush