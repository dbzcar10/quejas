@extends('layouts.admin', ['activePage' => 'quejas', 'titlePage' => __('Quejas')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-md-12">
            {{--<div class="row">
              <label class="pl-3">Filtrar por:</label>
              <div class="col-md-3">
                <label>Región</label>
                <select data-column="3" class="filter-select" id="select-region" style="width: 100%">
                  <option value="">Todas las regiones</option>
                    @foreach ($regiones as $region)
                      <option value="{{$region->nombre}}">{{$region->nombre}}</option>
                    @endforeach
                </select>
              </div>
              
              <div class="col-sm-3">
                <label>Departamento</label>
                <select data-column="4" class="filter-select" id="select-departamento" style="width: 100%">
                  <option value="">Todos los departamentos</option>
                    @foreach ($departamentos as $departamento)
                      <option value="{{$departamento->nombre}}">{{$departamento->nombre}}</option>
                    @endforeach
                </select>
              </div>
              <div class="col-sm-3">
                <label>Municipio</label>
                <select data-column="5" class="filter-select" id="select-municipio" style="width: 100%">
                  <option value="">Todos los municipios</option>
                    @foreach ($municipios as $municipio)
                      <option value="{{$municipio->nombre}}">{{$municipio->nombre}}</option>
                    @endforeach
                </select>
              </div>
              
            </div>--}}
            @livewire('select-filtro')
            <br>
            <form id="rptFechaForm">
                {{-- {{csrf_field()}} --}}
                <div class="row"><!--form-row-->
                    <div class="col-sm-4">
                        <label for="">Fecha de Inicio</label>
                        <div class="input-group mb-3">
                            <li class="input-group-text fa fa-calendar"></li><input class="form-control date_range_filter" name="fecha_inicio" id="fecha_inicio">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Fecha de Fin</label>
                        <div class="input-group mb-3">
                            <li class="input-group-text fa fa-calendar"></li><input class="form-control date_range_filter" name="fecha_fin" id="fecha_fin">
                        </div>
                    </div>                
                    <div class="col-sm-2 pt-4">
                      <button class="btn btn-primary" id="btnFiltrar">Filtrar por fecha</button>
                    </div>
                    <div class="col-sm-2 pt-4">
                      <button class="btn btn-info" id="btnReset">Reset</button>
                    </div>
                </div>

            </form>
          <br>
            <div class="card card-hidden mb-3">
                <div class="card-header card-header-primary text-center">
                    <h4>Quejas</h4>
                </div>

                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table" id="quejas-table">
                      <thead class=" text-primary">
                        <tr>
                          <th>ID</th>
                          <th>Fecha</th>
                          <th>Comercio</th>
                          <th>Región</th>
                          <th>Departamento</th>
                          <th>Municipio</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
          </div>
      </div>
  </div>
  <div class="loader loader-bar is-active"></div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
      /*$('#select-departamento').select2();
      $('#select-municipio').select2();
      $('#select-region').select2();*/

      //cargar_tabla();
    });

    //var quejas_table;

    $('.filter-select').change(function() {
      quejas_table.column($(this).data('column'))
          .search($(this).val())
          .draw();
    });

    /*function cargar_tabla(fecha_inicio = '', fecha_fin = '') {*/

      var quejas_table = $('#quejas-table').DataTable({
            ajax: {
              url: '{{route("quejas.getJson")}}',
              //data: {fecha_inicio: fecha_inicio, fecha_fin: fecha_fin}
              data: function(d){
                d.fecha_inicio = $('#fecha_inicio').val();
                d.fecha_fin = $('#fecha_fin').val();
              }  
            },
            "columns": [
              { "data": "id"},
              { "data": "fecha_queja"},
              { "data": "sucursal.comercio.nombre_comercio" },
              { "data": "sucursal.departamento.region.nombre" },
              { "data": "sucursal.departamento.nombre" },
              { "data": "sucursal.municipio.nombre" },
              { "data": "btn", orderable: false, searchable: false },
            ],

      });
    //}
        
    //**********Filtro de Fecha************/

    var fecha_inicio = new Pikaday({
        field: document.getElementById('fecha_inicio'),
        format: 'DD-MM-YYYY',
    });

    var fecha_fin = new Pikaday({
        field: document.getElementById('fecha_fin'),
        format: 'DD-MM-YYYY',
    });

    $('#rptFechaForm').on('submit', function(e) {
        //borrarSelects();
        quejas_table.draw();
        e.preventDefault();
    });
    
  /*$('#btnFiltrar').click(function(){

    borrarSelects();

    setTimeout(() => {
      var from_date = $('#fecha_inicio').val();
      var to_date = $('#fecha_fin').val();

      if(from_date != '' &&  to_date != '')
      {
        quejas_table.destroy();

        //cargar_tabla(from_date, to_date);

      }
      else
      {
        Swal.fire(
          'Error!',
          'Ingresa ambas fechas',
          'error'
        )
      }
    }, 250);

  });*/


  $('#btnReset').click(function(){
    $('#fecha_inicio').val('');
    $('#fecha_fin').val('');
    //borrarSelects();
    //quejas_table.destroy();
    //cargar_tabla();
    quejas_table.draw();
 });


 function borrarSelects(){
    $('#select-region').val('');
    $('#select-region').change();
    $('#select-departamento').val('');
    $('#select-departamento').change();
    $('#select-municipio').val('');
    $('#select-municipio').change();
 }

    document.addEventListener("DOMContentLoaded", () => {
        Livewire.on('municipio_vacio', a => {
          $('#select-municipio').val('');
          $('#select-municipio').change();
          console.log('Municipio Vacio');
        });

        Livewire.on('todos_departamentos', a => {
          $('#select-departamento').val('');
          $('#select-departamento').change();
          console.log('todos_departamentos');
        });
    });


        /*var fecha_inicio = new Pikaday({
            field: document.getElementById('fecha_inicio'),
            format: 'DD-MM-YYYY',
            onSelect: function(date) {
              minDateFilter = new Date(date).getTime();
              quejas_table.draw();
            }
        });

        $('#fecha_inicio').keyup(function() {
          minDateFilter = new Date(this.value).getTime();
          quejas_table.draw();
        })


        var fecha_fin = new Pikaday({
            field: document.getElementById('fecha_fin'),
            //format: 'DD-MM-YYYY',
            onSelect: function(date) {
              maxDateFilter = new Date(date).getTime();
              quejas_table.draw();
            }
        });

        $('#fecha_fin').keyup(function() {
          maxDateFilter = new Date(this.value).getTime();
          quejas_table.draw();
        })


    // Date range filter
    minDateFilter = "";
    maxDateFilter = "";

    $.fn.dataTable.ext.search.push(
      function(oSettings, aData, iDataIndex) {
        console.log(aData);
        if (typeof aData._date == 'undefined') {
          aData._date = new Date(aData[0]).getTime();
        }

        if (minDateFilter && !isNaN(minDateFilter)) {
          if (aData._date < minDateFilter) {
            return false;
          }
        }

        if (maxDateFilter && !isNaN(maxDateFilter)) {
          if (aData._date > maxDateFilter) {
            return false;
          }
        }

        return true;
      }
    );*/
  </script>
@endpush