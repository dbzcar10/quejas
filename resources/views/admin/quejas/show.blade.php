@extends('layouts.admin', ['activePage' => 'quejas.show', 'titlePage' => __('Detalle de Queja')])

@section('content')
  <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-md-12">
              <div class="card card-hidden mb-3">
                  <div class="card-header card-header-primary text-center">
                      <h4>Detalle de Queja</h4>
                  </div>
                  <div class="card-body">
                    <h6>Datos de Comercio</h6>
                    <div class="row">

                      <label for="name" class="col-sm-1 col-form-label">Nit</label>
                      <div class="col-sm-3">
                          <div class="form-group">
                            {{--<input type="text" readonly value="{{$queja->sucursal->comercio->nit}}" class="form-control">--}}
                            <p>{{$queja->sucursal->comercio->nit}}</p>
                          </div>
                      </div>
                      <label for="name" class="col-sm-1 col-form-label">Nombre Comercial</label>
                      <div class="col-sm-3">
                          <div class="form-group">
                            <p>{{$queja->sucursal->comercio->nombre_comercio}}</p>
                          </div>
                      </div>
                      <label for="name" class="col-sm-1 col-form-label">Razón Social</label>
                      <div class="col-sm-3">
                          <div class="form-group">
                            <p>{{$queja->sucursal->comercio->razon_social}}</p>
                          </div>
                      </div>                               
                    </div>
                    
                    <div class="row">
                      <label class="col-sm-1 col-form-label">Dirección</label>
                      <div class="col-sm-3">
                          <div class="form-group">
                            <p>{{$queja->sucursal->direccion}}</p>
                          </div>
                      </div>
                      <label class="col-sm-1 col-form-label">Teléfono</label>
                        <div class="col-sm-3">
                            <div class="form-group">
                              <p>{{$queja->sucursal->telefono}}</p>
                            </div>
                        </div>
                        <label class="col-sm-1 col-form-label">Email</label>
                        <div class="col-sm-3">
                            <div class="form-group">
                              <p>{{$queja->sucursal->correo}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-1 col-form-label">Departamento</label>
                      <div class="col-sm-3">
                          <div class="form-group">
                            <p>{{$queja->sucursal->departamento->nombre}}</p>
                          </div>
                      </div>
                      <label class="col-sm-1 col-form-label">Municipio</label>
                      <div class="col-sm-3">
                          <div class="form-group">
                            <p>{{$queja->sucursal->municipio->nombre}}</p>
                          </div>
                      </div>
                    </div>
                    <br>
                    <h6>Datos de Queja</h6>
                    <div class="row">
                      <label class="col-sm-1 col-form-label">No.Documento</label>
                      <div class="col-sm-3">
                          <div class="form-group">
                            <p>{{$queja->no_documento}}</p>
                          </div>
                      </div>
                      <label class="col-sm-1 col-form-label">Fecha Documento</label>
                      <div class="col-sm-3">
                          <div class="form-group">
                            <p>{{$queja->fecha_documento != null ? \Carbon\Carbon::createFromFormat('Y-m-d', $queja->fecha_documento)->format('d/m/y') : ''}}</p>
                          </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-1 col-form-label">Detalle de queja</label>
                      <div class="col-sm-11">
                          <div class="form-group">
                            <p>{{$queja->queja}}</p>
                          </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-1 col-form-label">Solicita</label>
                      <div class="col-sm-11">
                          <div class="form-group">
                            <p>{{$queja->solicitud}}</p>
                          </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-1 col-form-label">Documento</label>
                      <div class="col-sm-11">
                        @if($queja->documento)
                            <a href="{{route('quejas.documento', $queja)}}" class="btn btn-danger" 
                              target="_blank" 
                              title="Ver Documento" 
                              data-toggle="tooltip" 
                              data-placement="top">
                              <i class="material-icons md-36">picture_as_pdf</i>
                            </a>
                        @else
                            <p>Sin documento</p>
                        @endif
                      </div>
                    </div>
                  </div>
              </div>
              <div class="text-right m-t-15">
                <a href="{{route('quejas')}}" class="btn btn-default">Regresar</a>
            </div>
          </div>
      </div>
  </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
     
    });

  </script>
@endpush