@extends('layouts.reportes')

@section('title')
<title>Reporte General</title>
@endsection

@push('styles')
    <style>
        td{
            border: 1px solid black;
        }
    </style>    
@endpush

@section('contenido')
    <img src="{{public_path('img/LogoDiaco.jpg')}}" alt="" width="350">
    <h3 class="text-center">Reporte General de Quejas del {{$fecha_inicio->format('d/m/Y')}} al {{$fecha_fin->format('d/m/Y')}}</h3>
    <br>

    {{--Regiones--}}
    <div style="text-align:center;">
        <h4>Reporte por Departamentos</h4>
        <br>
        <table width="85%" style="margin: 0 auto" cellspacing="0" cellpadding="0">
            <tr>
                <th>Nombre</th>
                <th>Total Quejas</th>
            </tr>
            @foreach ($regiones as $region)
                <tr>
                    <td>{{$region->nombre}}</td>
                    <td class="text-center">{{DB::select($queryRegion, [$region->id, $fecha_inicio, $fecha_fin])[0]->total}}</td>
                </tr>
            @endforeach
    
        </table>
        <br>
    </div>

    {{--Departamentos--}}
    <div style="text-align:center;">
        <h4>Reporte por Departamentos</h4>
        <br>
        <table width="85%" style="margin: 0 auto" cellspacing="0" cellpadding="0">
            <tr>
                <th>Nombre</th>
                <th>Total Quejas</th>
            </tr>
            @foreach ($departamentos as $departamento)
                <tr>
                    <td>{{$departamento->nombre}}</td>
                    <td class="text-center">{{DB::select($queryDepa, [$departamento->id, $fecha_inicio, $fecha_fin])[0]->total}}</td>
                </tr>
            @endforeach
    
        </table>
        <br>
    </div>
    <br>
    {{--Municipios--}}
    <div style="text-align:center;">
        <h4>Reporte por Municipio</h4>
        <br>
        @foreach ($departamentos as $departamento)
            <h5 class="text-center">{{$departamento->nombre}}</h5>
            <br>
            <table width="85%" style="margin: 0 auto" >
                <tr>
                    <th>Nombre</th>
                    <th>Total Quejas</th>
                </tr>
                @php
                    $municipios1= $municipios->where('departamento_id', $departamento->id);
                @endphp

                @foreach ($municipios1 as $municipio)
                    <tr>
                        <td>{{$municipio->nombre}}</td>
                        <td class="text-center">{{DB::select($queryMuni, [$municipio->id, $fecha_inicio, $fecha_fin])[0]->total}}</td>
                    </tr>
                @endforeach
        
        
            </table>
            <br>
        @endforeach
        
    </div>
    

    

@endsection