<?php

use Illuminate\Support\Facades\Route;

/*DB::listen(function($query){
    echo "<pre> {$query->sql } </pre>";
});*/
  
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');
Route::get('users/verificar/{token}', [App\Http\Controllers\UsersController::class, 'verificar'])->name('verificar');
//Reenviar correo de confirmacion
Route::get('/users/{user}/resend',  [App\Http\Controllers\UsersController::class, 'resend'])->name('users.resend');
//Auth::routes();

// Authentication Routes...
Route::get('login', [App\Http\Controllers\Auth\LoginController::class,'showLoginForm'])->name('login');
Route::post('login', [App\Http\Controllers\Auth\LoginController::class,'login']);
Route::post('logout', [App\Http\Controllers\Auth\LoginController::class,'logout'])->name('logout');
Route::get('password/reset', [App\Http\Controllers\Auth\ForgotPasswordController::class,'showLinkRequestForm'])->name('password.request');
Route::post('password/email', [App\Http\Controllers\Auth\ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');
Route::post('password/reset', [App\Http\Controllers\Auth\ResetPasswordController::class,'reset'])->name('password.update');
Route::get('password/reset/{token}', [App\Http\Controllers\Auth\ResetPasswordController::class,'showResetForm'])->name('password.reset');

Route::group(['middleware' => ['auth', 'verificado', 'estado']],
function(){
    //Users
    Route::get('users', [App\Http\Controllers\UsersController::class, 'index'])->name('users');
    Route::get('users/getJson', [App\Http\Controllers\UsersController::class, 'getJson'])->name('users.getJson');
    Route::get('users/create' , [App\Http\Controllers\UsersController::class, 'create'] )->name('users.create');
    Route::POST('users' , [App\Http\Controllers\UsersController::class, 'store'] )->name('users.store');
    Route::get('users/{usuario}' , [App\Http\Controllers\UsersController::class, 'edit'] )->name('users.edit');
    Route::put('users/{usuario}' , [App\Http\Controllers\UsersController::class, 'update'] )->name('users.update');
    Route::POST('users/reset' , [App\Http\Controllers\UsersController::class, 'resetPassword'])->name('users.reset');//Cambiar Contraseña
    Route::delete('users/{usuario}' , [App\Http\Controllers\UsersController::class, 'desactivar'] )->name('users.desactivar');
    Route::post('users/activar/{usuario}' , [App\Http\Controllers\UsersController::class, 'activar'] )->name('users.activar');

    //Quejas
    Route::get('quejas', [App\Http\Controllers\QuejasController::class, 'index'])->name('quejas');
    Route::get('quejas/getJson', [App\Http\Controllers\QuejasController::class, 'getJson'])->name('quejas.getJson');
    Route::get('quejas/{queja}/show', [App\Http\Controllers\QuejasController::class, 'show'])->name('quejas.show');
    Route::get('quejas/{queja}/documento', [App\Http\Controllers\QuejasController::class, 'documento'])->name('quejas.documento');

    //Comercios
    Route::get('comercios', [App\Http\Controllers\ComerciosController::class, 'index'])->name('comercios');
    Route::get('comercios/getJson', [App\Http\Controllers\ComerciosController::class, 'getJson'])->name('comercios.getJson');
    Route::get('comercios/create', [App\Http\Controllers\ComerciosController::class, 'create'])->name('comercios.create');
    Route::post('comercios/store', [App\Http\Controllers\ComerciosController::class, 'store'])->name('comercios.store');
    Route::get('comercios/{comercio}/show', [App\Http\Controllers\ComerciosController::class, 'show'])->name('comercios.show');
    Route::get('comercios/edit/{comercio}', [App\Http\Controllers\ComerciosController::class, 'edit'])->name('comercios.edit');
    Route::put('comercios/{comercio}', [App\Http\Controllers\ComerciosController::class, 'update'])->name('comercios.update');
    Route::delete('comercios/destroy/{comercio}', [App\Http\Controllers\ComerciosController::class, 'destroy'])->name('comercios.destroy');

    //Sucursales
    Route::get('sucursales/getjsonComercio/{comercio}', [App\Http\Controllers\SucursalesController::class, 'getJsonComercio'])->name('sucursales.getJsonComercio');
    Route::delete('sucursales/destroy/{sucursal}', [App\Http\Controllers\SucursalesController::class, 'destroy'])->name('sucursales.destroy');
    Route::get('sucursales/create/{comercio}', [App\Http\Controllers\SucursalesController::class, 'create'])->name('sucursales.create');
    Route::post('sucursales', [App\Http\Controllers\SucursalesController::class, 'store'])->name('sucursales.store');
    Route::get('sucursales/edit/{sucursal}/{comercio}', [App\Http\Controllers\SucursalesController::class, 'edit'])->name('sucursales.edit');
    Route::put('sucursales/{sucursal}', [App\Http\Controllers\SucursalesController::class, 'update'])->name('sucursales.update');

    //Reportes
    Route::get('reportes/general' , [App\Http\Controllers\ReportesController::class, 'general'])->name('reportes.general');
    Route::get('reportes/por_fecha' , [App\Http\Controllers\ReportesController::class, 'por_fecha'])->name('reportes.por_fecha');
    Route::post('reportes/por_fecha/grafica' , [App\Http\Controllers\ReportesController::class, 'por_fecha_grafica'])->name('reportes.por_fecha_grafica');

    Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');
    Route::get('/graficaLineal', [App\Http\Controllers\HomeController::class, 'graficaLineal'])->name('graficaLineal');
});







