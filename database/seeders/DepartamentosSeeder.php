<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Departamento;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departamento=new Departamento();
        $departamento->region_id=1;
        $departamento->nombre="Guatemala";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=3;
        $departamento->nombre="El Progreso";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=5;
        $departamento->nombre="Sacatepequez";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=5;
        $departamento->nombre="Chimaltenango";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=5;
        $departamento->nombre="Escuintla";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=4;
        $departamento->nombre="Santa Rosa";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=6;
        $departamento->nombre="Sololá";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=6;
        $departamento->nombre="Totonicapán";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=6;
        $departamento->nombre="Quetzaltenango";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=6;
        $departamento->nombre="Suchitepéquez";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=6;
        $departamento->nombre="Retalhuleu";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=6;
        $departamento->nombre="San Marcos";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=7;
        $departamento->nombre="Huehuetenango";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=7;
        $departamento->nombre="Quiché";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=2;
        $departamento->nombre="Baja Verapaz";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=2;
        $departamento->nombre="Alta Verapaz";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=8;
        $departamento->nombre="Petén";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=3;
        $departamento->nombre="Izabal";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=3;
        $departamento->nombre="Zacapa";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=3;
        $departamento->nombre="Chiquimula";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=4;
        $departamento->nombre="Jalapa";
        $departamento->save();

        $departamento=new Departamento();
        $departamento->region_id=4;
        $departamento->nombre="Jutiapa";
        $departamento->save();
    }
}
