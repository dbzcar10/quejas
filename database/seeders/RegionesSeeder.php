<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Region;

class RegionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $region=new Region();
        $region->nombre="Región Metropolitana";
        $region->save();

        $region=new Region();
        $region->nombre="Región Norte";
        $region->save();

        $region=new Region();
        $region->nombre="Región Nororiental";
        $region->save();

        $region=new Region();
        $region->nombre="Región Suroriental";
        $region->save();

        $region=new Region();
        $region->nombre="Región central";
        $region->save();

        $region=new Region();
        $region->nombre="Región Suroccidental";
        $region->save();

        $region=new Region();
        $region->nombre="Región Noroccidental";
        $region->save();

        $region=new Region();
        $region->nombre="Región Petén";
        $region->save();
    }
}
