<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Storage::deleteDirectory('documentos');
        Storage::deleteDirectory('livewire-tmp');
        // \App\Models\User::factory(10)->create();
        $this->call(UsersSeeder::class);
        $this->call(RegionesSeeder::class);
        $this->call(DepartamentosSeeder::class);
        $this->call(MunicipiosSeeder::class);
        
    }
}
