<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::flushEventListeners();
        //Usuarios
        $user = new User;
        $user->name = 'Super Administrador';
        $user->email= 'cardx00@gmail.com';
        $user->password = bcrypt('admin');
        $user->email_verified_at = now();
        $user->verification_token = null;
        $user->save();
    }
}
