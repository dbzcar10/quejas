<?php

namespace Database\Factories;

use App\Models\Comercio;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComercioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comercio::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nit' => $this->faker->unique()->randomNumber(8),
            'nombre_comercio' => $comercio = $this->faker->company('es_ES'),
            'razon_social' => $comercio ." "."S.A",
        ];
    }
}
