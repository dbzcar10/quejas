<?php

namespace Database\Factories;

use App\Models\Sucursal;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Departamento;
use App\Models\Municipio;
use App\Models\Comercio;

class SucursalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sucursal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'direccion' => $this->faker->address('es_ES'),
            'telefono' => $this->faker->numerify('########'),
            'correo' => $this->faker->email(),
            'departamento_id' => $depa =  Departamento::all()->random()->id,
            'municipio_id' => Municipio::where('departamento_id', $depa)->get()->random()->id,
            //'comercio_id' => Comercio::all()->random()->id,
            'comercio_id' => Comercio::all()->random()->id,
        ];
    }
}
