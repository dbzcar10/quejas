<?php

namespace Database\Factories;

use App\Models\Queja;
use App\Models\Sucursal;
use App\Models\Documento;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class QuejaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Queja::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'queja' => $this->faker->text(255),
            'solicitud' => $this->faker->text(255),
            'fecha_documento' => null,
            'no_documento' => null,
            'fecha_queja' => $this->faker->dateTimeBetween($startDate = Carbon::now()->subYears(1), $endDate = Carbon::now(), $timezone = null),
            'sucursal_id' => Sucursal::all()->random()->id,
        ];
    }

    public function conDocumento()
    {
        return $this->state(function (array $attributes) {
            return [
                'no_documento' => $this->faker->randomNumber(8), 
                'fecha_documento' => $fecha_documento = $this->faker->dateTimeBetween($startDate = Carbon::now()->subYears(1), $endDate = Carbon::now(), $timezone = null),
                'fecha_queja' => $fecha_documento,
            ];
        });
    }
}
