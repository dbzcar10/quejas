<?php

namespace Database\Factories;

use App\Models\Documento;
use App\Models\Queja;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

class DocumentoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Documento::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        Storage::put('documentos/DocumentoPrueba.pdf', file_get_contents( public_path('DocumentoPrueba.pdf') ) );
        return [
            'queja_id' => Queja::factory()->conDocumento(),
            'nombre' => 'DocumentoPrueba.pdf',
            'url' => 'documentos/DocumentoPrueba.pdf'
        ];
    }
}
