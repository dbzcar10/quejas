// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

/* Create map instance */
var chart = am4core.create("chartdiv", am4maps.MapChart);
chart.chartContainer.wheelable = false;

/* Set map definition */
chart.geodata = am4geodata_guatemalaLow;

/* Set projection */
chart.projection = new am4maps.projections.Mercator();

/* Create map polygon series */
//var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
chart.responsive.enabled = true;

/* Make map load polygon (like country names) data from GeoJSON */
//polygonSeries.useGeodata = true;

/* Configure series */
/*var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.togglable = true;
polygonTemplate.tooltipText = "{name}";
polygonTemplate.nonScalingStroke = true;
polygonTemplate.strokeOpacity = 0.5;
polygonTemplate.fill = '#D5DCD5';*/


/*var hs = polygonTemplate.states.create("hover");
hs.properties.fill = chart.colors.getIndex(4);*/


// Small map
/*chart.smallMap = new am4maps.SmallMap();
// Re-position to top right (it defaults to bottom left)
chart.smallMap.align = "right";
chart.smallMap.valign = "top";
chart.smallMap.series.push(polygonSeries);*/

// Zoom control
chart.zoomControl = new am4maps.ZoomControl();

var homeButton = new am4core.Button();
homeButton.events.on("hit", function(){
  chart.goHome();
});

homeButton.icon = new am4core.Sprite();
homeButton.padding(7, 5, 7, 5);
homeButton.width = 30;
homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
homeButton.marginBottom = 10;
homeButton.parent = chart.zoomControl;
homeButton.insertBefore(chart.zoomControl.plusButton);




// Create a series for each group, and populate the above array
groupData.forEach(function(group) {
  var series = chart.series.push(new am4maps.MapPolygonSeries());
  series.name = group.name;
  series.useGeodata = true;
  var includedCountries = [];
  group.data.forEach(function(country) {
    includedCountries.push(country.id);
  });
  series.include = includedCountries;

  series.fill = am4core.color(group.color);

  series.setStateOnChildren = true;
  series.calculateVisualCenter = true;

  // Country shape properties & behaviors
  var mapPolygonTemplate = series.mapPolygons.template;
  // Instead of our custom title, we could also use {name} which comes from geodata  
  mapPolygonTemplate.fill = am4core.color(group.color);
  mapPolygonTemplate.fillOpacity = 0.8;
  mapPolygonTemplate.nonScalingStroke = true;
  mapPolygonTemplate.tooltipPosition = "fixed"

  mapPolygonTemplate.events.on("over", function(event) {
    series.mapPolygons.each(function(mapPolygon) {
      mapPolygon.isHover = true;
    })
    event.target.isHover = false;
    event.target.isHover = true;
  })

  mapPolygonTemplate.events.on("out", function(event) {
    series.mapPolygons.each(function(mapPolygon) {
      mapPolygon.isHover = false;
    })
  })

  // States  
  var hoverState = mapPolygonTemplate.states.create("hover");
  hoverState.properties.fill = am4core.color("#CC0000");

  // Tooltip
  mapPolygonTemplate.tooltipText = group.name +" "+group.totalQuejas + " quejas"; // enables tooltip
  //mapPolygonTemplate.tooltipText = group.name +"joined EU at {customData}"; // enables tooltip
   series.tooltip.getFillFromObject = false; // prevents default colorization, which would make all tooltips red on hover
   //series.tooltip.background.fill = am4core.color(group.color);
   series.tooltip.background.fill = am4core.color('#CC0000');

  // MapPolygonSeries will mutate the data assigned to it, 
  // we make and provide a copy of the original data array to leave it untouched.
  // (This method of copying works only for simple objects, e.g. it will not work
  //  as predictably for deep copying custom Classes.)
  series.data = JSON.parse(JSON.stringify(group.data));
});

/* Create legend */
chart.legend = new am4maps.Legend();
//chart.legend.paddingLeft = 27;
//chart.legend.paddingRight = 27;
chart.legend.paddingTop = 10;
//chart.legend.width = am4core.percent(90);
//chart.legend.valign = "bottom";
//chart.legend.contentAlign = "left";
//chart.legend.position = "right";
//chart.legend.align = "right";
// Legend items
//chart.legend.itemContainers.template.interactionsEnabled = false;

/* Create a separate container to put legend in */
var legendContainer = am4core.create("legenddiv", am4core.Container);
legendContainer.width = am4core.percent(100);
legendContainer.height = am4core.percent(100);
chart.legend.parent = legendContainer;