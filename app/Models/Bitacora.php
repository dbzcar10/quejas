<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Bitacora extends Model
{
    use HasFactory;

    protected $table = 'bitacoras';

    protected $fillable = [
        'modelo_id',
        'user_id',
        'accion',
        'info_anterior',
        'info_nueva',
        'nombre_tabla'
    ];

    public function createBitacora(array $data) {

        $bitacora = new Bitacora;
        $bitacora->user_id = auth()->user()->id;
        $bitacora->modelo_id = $data['modelo_id'];
        $bitacora->nombre_tabla = $data['nombre_tabla'];
        $bitacora->accion = $data['accion'];
        $bitacora->info_anterior = $data['info_anterior'];
        $bitacora->info_nueva = $data['info_nueva'];

        return DB::transaction( function () use ($bitacora, $data) {
            $bitacora->save();
        });
    }
}
