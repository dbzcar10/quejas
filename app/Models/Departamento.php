<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    use HasFactory;

    protected $table = 'departamentos';

    protected $fillable = ['nombre', 'region_id'];

    public function municipios()
    {
        return $this->hasMany(Municipio::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
