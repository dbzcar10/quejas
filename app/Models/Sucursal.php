<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sucursal extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='sucursales';

    protected $fillable=['direccion', 'departamento_id', 'municipio_id', 'telefono', 'correo', 'comercio_id'];


    public function comercio()
    {
        return $this->belongsTo(Comercio::class);
    }

    public function departamento()
    {
        return $this->belongsTo(Departamento::class);
    }

    public function municipio()
    {
        return $this->belongsTo(Municipio::class);
    }

    public function quejas()
    {
        return $this->hasMany(Queja::class);
    }
}
