<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Queja extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'quejas';
    protected $fillable = ['queja', 'sucursal_id', 'fecha_documento', 'no_documento', 'solicitud', 'fecha_queja'];

    protected $dates = ['fecha_queja'];

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    public function documento()
    {
        return $this->hasOne(Documento::class);
    }
}
