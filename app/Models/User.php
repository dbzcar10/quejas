<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\MailResetPasswordToken;
use App\Mail\ResetPasswordToken;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'verification_token',
        'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function generarVerificationToken()
    {
        return Str::random(40);
    }

    public function esVerificado()
    {
        return $this->email_verified_at != null;
    }

    public function sendPasswordResetNotification($token)
    {

        //$this->notify(new MailResetPasswordToken($token));

        //\Notification::sendNow($this, new MailResetPasswordToken($token));

        Mail::to($this)->send(new ResetPasswordToken($token));

    }
}
