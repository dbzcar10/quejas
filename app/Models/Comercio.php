<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comercio extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='comercios';

    protected $fillable=['nit', 'razon_social', 'nombre_comercio'];


    public function sucursales()
    {
        return $this->hasMany(Sucursal::class);
    }
}
