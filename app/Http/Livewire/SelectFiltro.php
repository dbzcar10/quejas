<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Departamento;
use App\Models\Municipio;
use App\Models\Region;

class SelectFiltro extends Component
{
    public $departamento;
    public $departamentos=[];
    public $municipio;
    public $region;
    public $municipios=[];


    public function render()
    {
        if(!empty($this->region)) {
            $this->departamentos = Departamento::join('regiones', 'departamentos.region_id', '=', 'regiones.id')
                                        ->where('regiones.nombre', $this->region)
                                        ->select('departamentos.*')->get();
        }else{
            $this->departamentos = Departamento::orderBy('nombre')->get();
        }

        return view('livewire.select-filtro')
            ->withRegiones(Region::orderBy('nombre')->get());
        
    }

    public function updatedDepartamento($departamento){
        $this->municipios = [];
        $this->emit('municipio_vacio');
        if(!empty($departamento)) {
            $this->municipios = Municipio::join('departamentos', 'municipios.departamento_id', '=', 'departamentos.id')
                                        ->where('departamentos.nombre', $departamento)
                                        ->select('municipios.*')->get();
        }
    }

    public function updatedRegion($region){
        if(!empty($region)) {
            $this->departamentos = Departamento::orderBy('nombre')->get();
            $this->municipios = [];
            $this->emit('municipio_vacio');
            $this->emit('todos_departamentos');
        }else{
            $this->departamentos = Departamento::orderBy('nombre')->get();
            $this->municipios = [];
            $this->emit('municipio_vacio');
            $this->emit('todos_departamentos');
        }

        return view('livewire.select-filtro')
            ->withRegiones(Region::orderBy('nombre')->get());
    }
}
