<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Departamento;
use App\Models\Municipio;

class SelectDepartamentoForm extends Component
{
    public $departamento;
    public $municipio;
    public $municipios=[];


    public function render()
    {
        if(!empty($this->departamento)) {
            $this->municipios = Municipio::where('departamento_id', $this->departamento)->get();
        }
        return view('livewire.select-departamento-form')
            ->withDepartamentos(Departamento::orderBy('nombre')->get());
        
    }

    public function mount($departamento = '', $municipio = '')
    {
        if($departamento == null || $departamento == ''){
            $this->departamento = request()->old('departamento');
        }else{
            $this->departamento = $departamento;
        }

        if($municipio == null || $municipio == ''){
            $this->municipio = request()->old('municipio');
        }else{
            $this->municipio = $municipio;
        }
        
        //$this->municipio = request()->old('municipio');
    }
}
