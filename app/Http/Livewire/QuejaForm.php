<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Departamento;
use App\Models\Municipio;
use App\Models\Comercio;
use App\Models\Sucursal;
use App\Models\Queja;
use App\Models\Documento;
use App\Models\Bitacora;
use DB;
use Exception;
use Carbon\Carbon;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class QuejaForm extends Component
{
    use WithFileUploads;

    public $nit;
    public $razon_social;
    public $nombre_comercial;
    public $direccion;
    public $telefono;
    public $correo = '';
    public $queja;
    public $solicitud;
    public $fecha_documento;
    public $no_documento;
    public $documento = '';


    public $departamento;
    public $municipio;
    public $municipios=[];
    
    /*public function mount($departamento, $municipio)
    {
        $this->departamento = $departamento;
        $this->municipio = $municipio;
    }*/

    public function mount()
    {
        $this->departamento = request()->old('departamento');
        $this->municipio = request()->old('municipio');
    }

    public function render()
    {
        if(!empty($this->departamento)) {
            $this->municipios = Municipio::where('departamento_id', $this->departamento)->get();
        }
        return view('livewire.queja-form')
            ->withDepartamentos(Departamento::orderBy('nombre')->get());
    }

    public function updated($campos)
    {
        $this->validateOnly($campos, $this->rules);
    }

    public function updatedNit($nit){
        $comercio = Comercio::where('nit', $nit)->first();

        if($comercio){

            $this->razon_social = $comercio->razon_social;
            $this->nombre_comercial = $comercio->nombre_comercio;
        }
    }

    protected $messages = [
        'queja.required' => 'El campo detalle de queja es obligatorio.',
    ];

    /*public function updatedDepartamento($departamento_id)
    {
        $this->municipios = Municipio::where('departamento_id', $this->departamento_id)->get();
    }*/

    protected $rules = [
        'nit' => 'required|min:6|max:10',
        'razon_social' => 'nullable|max:100',
        'nombre_comercial' => 'required|max:100',
        'direccion' => 'required|max:255',
        'telefono' => ['nullable','max:8', 'regex:/([0-9]){8}/'],
        'correo' => 'nullable|email|max:100',
        'departamento' => 'required',
        'municipio' => 'required',
        'queja' => 'required',
        'solicitud' => 'required',
        'documento' => 'mimetypes:application/pdf|max:2048|required_with:no_documento',
        'fecha_documento' => ['nullable', 'date',  'required_with:no_documento'],
        'no_documento' => 'nullable|max:30|required_with:fecha_documento'
    ];

    public function submit()
    {
        $validateData = $this->validate();

        try{

            DB::beginTransaction();
            //Valores a ingresar
            $data['nit'] = $this->nit;
            $data['razon_social'] = $this->razon_social;
            $data['nombre_comercio'] = $this->nombre_comercial;

            $comercio = Comercio::firstOrCreate([
                'nit' => $this->nit
            ], $data);

            //Datos Sucursal
            $data2['comercio_id'] = $comercio->id;
            $data2['departamento_id'] = $this->departamento;
            $data2['municipio_id'] = $this->municipio;
            $data2['telefono'] = $this->telefono;
            $data2['correo'] = $this->correo == '' ? null: $this->correo ;

            $sucursal = Sucursal::firstOrCreate([
                'comercio_id' => $comercio->id,
                'municipio_id' => $this->municipio,
                'direccion' => $this->direccion,
            ], $data2);
            
            //Datos de queja
            $data3['no_documento'] = $this->no_documento;
            $data3['fecha_documento'] = Carbon::parse($this->fecha_documento);
            $data3['fecha_queja'] = Carbon::now();
            $data3['queja'] = $this->queja;
            $data3['solicitud'] = $this->solicitud;
            $data3['sucursal_id'] = $sucursal->id;


            $queja = Queja::create($data3);

            $user_id = null;
            if(auth()->check())
            {
                $user_id = auth()->user()->id;
            }

            //Bitácora
            $bitacora = new Bitacora;
            $bitacora->user_id = $user_id;
            $bitacora->modelo_id = $queja->id;
            $bitacora->nombre_tabla = 'quejas';
            $bitacora->accion = 'agregar';
            $bitacora->info_anterior = '';
            $bitacora->info_nueva = $queja;
            $bitacora->save();

            if($this->documento != null){
                $documentoUrl = $this->documento->store('documentos');
                $nombre_documento = Str::of($documentoUrl)->after('documentos/');

                Documento::create([
                    'nombre' => $nombre_documento,
                    'url' => $documentoUrl,
                    'queja_id' => $queja->id
                ]);
            }

            DB::commit();

            $this->reset();

            $this->emit('message', 'Su queja se registro correctamente.');

            //session()->flash('mensaje', 'Queja guardada exitosamente!');

            //return redirect()->to('/');

        }
        catch(Exception $ex){
            DB::rollBack();
            $this->emit('error', 'Ocurrio un problema intente de nuevo', $ex);
            //$this->emit('error', $ex);
        }        
    }



    
}
