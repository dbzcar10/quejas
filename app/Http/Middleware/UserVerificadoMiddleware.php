<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UserVerificadoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check() && auth()->user()->esVerificado() )
        return $next($request);

        else {
            auth()->logout();
            return redirect()
            ->route('login')
            ->with('MensajeEstado','El usuario ingresado no esta verificado');
        }
    }
}
