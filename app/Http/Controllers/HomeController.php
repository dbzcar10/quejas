<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Arr;
use App\Models\Queja;
use App\Models\Comercio;
use App\Models\Sucursal;
use Carbon\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $quejas = Queja::all();
        $query = 'SELECT COUNT(*) AS total FROM quejas Q INNER JOIN sucursales S ON S.id = Q.sucursal_id
        INNER JOIN departamentos D ON D.id = S.departamento_id
        INNER JOIN regiones R ON R.id = D.region_id
        AND R.id = ? AND Q.deleted_at IS NULL';

        $comercios = Comercio::all();
        $sucursales = Sucursal::all();

        $quejasHoy = DB::select('SELECT count(*) as total FROM quejas Q Where Q.fecha_queja = CURDATE() AND Q.deleted_at IS NULL');

        return view('admin.dashboard', compact('quejas', 'query', 'comercios', 'sucursales', 'quejasHoy'));
    }

    public function graficaLineal()
    {
        /*select 
          MONTH(C.fecha_emision) mes, YEAR(C.fecha_emision) year, COUNT(C.id) cantidad
      from 
          certificados C
      group BY 1, 2 ORDER BY YEAR desc, mes desc  LIMIT 3*/

        $results = DB::select( 'SELECT MONTH(Q.fecha_queja) mes,COUNT(Q.id) cantidad
                    from quejas Q WHERE YEAR(Q.fecha_queja)=year(CURDATE()) AND Q.deleted_at IS NULL group BY 1' );
            
        $results = Arr::pluck($results,'cantidad','mes');

        $quejas=[];
        for($i=1;$i<=12;$i++){
            $quejas[$i]= isset($results[$i]) ? $results[$i] : 0 ;
        }

        $ultimaQueja = Queja::all('fecha_queja')->max('fecha_queja');

        return response()->json(['data' => [
            'quejas' => $quejas,
            'ultimaQueja' => empty($ultimaQueja) ? '' : $ultimaQueja->format('d-m-Y H:m')
        ]
       ]);
    }
}
