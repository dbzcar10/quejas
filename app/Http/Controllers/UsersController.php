<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Bitacora;
use Illuminate\Validation\Rule;
use DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserCreated;

class UsersController extends Controller
{
    public function __construct(Bitacora $bitacora){
        $this->bitacora = $bitacora;
    }

    public function index()
    {
        return view('admin.users.index');
    }

    public function getJson(Request $request)
    {
        //$this->authorize('viewAny', User::class);
        if(!$request->ajax()) return abort('403');

        return datatables()
        ->eloquent(User::query() )
        ->editColumn('email_verified_at', function ($data) {
            $html = ($data->email_verified_at) ? '<div class="text-center"><span class="badge badge-success">Si <i class="fa fa-check"></i></span></div>' : '<div class="text-center"><span class="badge badge-danger">No <i class="fa fa-times-circle"></i></span></div>';
            return $html;
        })
        ->editColumn('activo', function ($data) {
            $html = ($data->activo) ? '<div class="text-center"><span class="badge badge-success">Activo <i class="fa fa-check"></i></span></div>' : '<div class="text-center"><span class="badge badge-danger">Inactivo <i class="fa fa-times-circle"></i></span></div>';
            return $html;
        })
        ->addColumn('btn', 'admin.users.acciones')
        ->rawColumns(['btn', 'email_verified_at', 'activo'])
        ->make(true);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {

        $messages = [
            'regex' => 'La contraseña debe contener al menos una mayúscula, una minúscula, un número y un carácter especial: @#%'
        ];

        $reglas = [
            'name' => 'required|max:100',
            'email' => 'required|email|max:100|unique:users',
            'password' => ['regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^0-9a-zA-Z])([A-Za-z]|[^ ]){8,15}$/', 'confirmed', 'min:8', 'required', 'max:150']
        ];

        
            $campos = $request->validate($reglas, $messages);

            $campos['password'] = bcrypt($request->password);
            $campos['email_verified_at'] = null;
            $campos['verification_token'] = User::generarVerificationToken();

            $usuario = User::create($campos);
            $usuario->save();

        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $usuario->id, 'nombre_tabla' => 'users', 'accion' => 'agregar', 'info_anterior' => '', 'info_nueva' => $usuario]);

        return redirect()->route('users')->withNotificacion(['mensaje' => 'El usuario ha sido creado', 'tipo' => 'success', 'icon' => 'check']);

    }

    public function edit(User $usuario)
    {
        //$this->authorize('update', $usuario);

        return view('admin.users.edit', compact('usuario'));
    }

    public function update(Request $request, User $usuario)
    {
        //$this->authorize('update', $usuario);

        $usuarioA = $usuario;


        $reglas = [
            'name' => 'required|max:100',
            'email' => ['required', 'email', 'max:100', Rule::unique('users')->ignore($usuario->id)],
        ];

            $campos = $request->validate($reglas);

            if($request->has('email') && $usuario->email != $request->email){
                $campos['email_verified_at'] = null;
                $campos['verification_token'] = User::generarVerificationToken();
                $campos['email'] = $request->email;
            }
            $usuario->update($campos);

        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $usuario->id, 'nombre_tabla' => 'users', 'accion' => 'editar', 'info_anterior' => $usuarioA, 'info_nueva' => $usuario]);

        return redirect()->route('users')->withNotificacion(['mensaje' => 'El usuario ha sido actualizado', 'tipo' => 'success', 'icon' => 'check']);
    }

    public function verificar(Request $request, $token){
        $user = User::where('verification_token', $token)->firstOrFail();

        $user->email_verified_at = now();
        $user->verification_token = null;
        $user->save();

        Auth::loginUsingId($user->id);
        return redirect()->route('dashboard')->withNotificacion(['mensaje' => 'La cuenta ha sido verificada.', 'tipo' => 'success', 'icon' => 'check']);
    }

    public function resend (User $user)
    {
        if ($user->esVerificado()){
            return redirect()->route('users')->withNotificacion(['mensaje' => 'Este usuario ya ha sido verificado.', 'tipo' => 'warning', 'icon' => 'warning']);
        }

        Mail::to($user)->send(new UserCreated($user));

        return redirect()->route('users')->withNotificacion(['mensaje' => 'El correo de verificación se ha enviado.', 'tipo' => 'success', 'icon' => 'check']);
    }
    //Cambiar contraseña
    public function resetPassword(Request $request)
    {
        $password_actual = Auth::user()->password;
        $user = User::where('id', auth()->id())->first();

        $data = $request->all();

        $messages = [
            'regex' => 'La contraseña debe contener al menos una mayuscula, una minuscula, un número y un caracter especial: @#%'
        ];

        $errors = Validator::make($data,[
            'password' => ['regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^0-9a-zA-Z])([A-Za-z]|[^ ]){8,15}$/', 'confirmed', 'min:8', 'required', 'max:150'],
        ], $messages);

        if($errors->fails())
        {
            return  Response::json($errors->errors(), 422);
        }

        if(password_verify($data['password_anterior'],$password_actual))
        {
            $userA = $user;
            $user->password = bcrypt($data['password']);
            $user->save();

            //Bitácora
            $this->bitacora->createBitacora(['modelo_id' => $user->id, 'nombre_tabla' => 'users', 'accion' => 'editar', 'info_anterior' => $userA, 'info_nueva' => $user]);
            
            return Response::json(['success' => 'Redirigir'], 200);
        }

        else{
        return  Response::json(['password_anterior' => 'La contraseña no coincide'], 422);
        }

    }

    public function desactivar(User $usuario)
    {

        $usuarioA = $usuario;
        $usuario->activo = false;
        $usuario->save();

        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $usuario->id, 'nombre_tabla' => 'users', 'accion' => 'desactivar', 'info_anterior' => $usuarioA, 'info_nueva' => $usuario]);

        if(auth()->user()->id == $usuario->id)
        {
            return Response::json(['success' => 'Redirigir'], 200);
        }else{
            return Response::json(['data' => 'ok'], 200);
        }
    }

    public function activar(User $usuario)
    {

        $usuarioA = $usuario;
        $usuario->activo = true;
        $usuario->save();

        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $usuario->id, 'nombre_tabla' => 'users', 'accion' => 'activar', 'info_anterior' => $usuarioA, 'info_nueva' => $usuario]);

        return Response::json(['data' => 'ok'], 200);
    }
}
