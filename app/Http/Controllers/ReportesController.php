<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Region;
use App\Models\Departamento;
use App\Models\Municipio;
use App\Models\Queja;
use Mpdf\Mpdf;
use Carbon\Carbon;

class ReportesController extends Controller
{
    public function general(Request $request)
    {
        set_time_limit(0);
        ini_set("pcre.backtrack_limit", "5000000");

        $queryRegion = 'SELECT COUNT(*) AS total FROM quejas Q INNER JOIN sucursales S ON S.id = Q.sucursal_id
        INNER JOIN departamentos D ON D.id = S.departamento_id
        INNER JOIN regiones R ON R.id = D.region_id
        AND R.id = ? AND Q.deleted_at IS NULL';

        $queryDepa = 'SELECT COUNT(*) AS total FROM quejas Q INNER JOIN sucursales S ON S.id = Q.sucursal_id
        AND S.departamento_id = ? AND Q.deleted_at IS NULL';

        $queryMuni = 'SELECT COUNT(*) AS total FROM quejas Q INNER JOIN sucursales S ON S.id = Q.sucursal_id
        AND S.municipio_id = ? AND Q.deleted_at IS NULL';


        $regiones = Region::all();
        $departamentos = Departamento::all();
        $municipios = Municipio::all();
        
    
        $html = view('pdf.RP_General', compact('departamentos', 'municipios', 'regiones', 'queryRegion', 'queryDepa', 'queryMuni') )->render();


        $mpdf = new Mpdf([
            'tempDir'=>storage_path('/app/public/tempdir'),
            // "format" => "A4",
             "format" => 'letter',
            //'format' => [215.9, 279.4],
            'margin_left' => 15,
            'margin_right' => 15,
            'margin_top' => 10,
            'margin_bottom' => 10,
            //'orientation' => 'L'
        ]);

        //$mpdf->SetTopMargin(7.5);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output('ReporteGeneralQuejas.pdf', "I");
    }

    public function por_fecha(Request $request)
    {
        return view('admin.reportes.por_fecha');
    }

    public function por_fecha_grafica(Request $request)
    {
        set_time_limit(0);
        ini_set("pcre.backtrack_limit", "5000000");

        $reglas = [
            'fecha_inicio' => 'date',
            'fecha_fin' => 'date',
        ];
        $campos = $request->validate($reglas);

        $fecha_inicio = new Carbon($request->fecha_inicio);
        $fecha_fin = new Carbon($request->fecha_fin);

        $queryRegion = 'SELECT COUNT(*) AS total FROM quejas Q INNER JOIN sucursales S ON S.id = Q.sucursal_id
        INNER JOIN departamentos D ON D.id = S.departamento_id
        INNER JOIN regiones R ON R.id = D.region_id
        AND R.id = ? AND Q.deleted_at IS NULL AND Q.fecha_queja BETWEEN ? AND ?';

        $queryDepa = 'SELECT COUNT(*) AS total FROM quejas Q INNER JOIN sucursales S ON S.id = Q.sucursal_id
        AND S.departamento_id = ? AND Q.deleted_at IS NULL AND Q.fecha_queja BETWEEN ? AND ?';

        $queryMuni = 'SELECT COUNT(*) AS total FROM quejas Q INNER JOIN sucursales S ON S.id = Q.sucursal_id
        AND S.municipio_id = ? AND Q.deleted_at IS NULL AND Q.fecha_queja BETWEEN ? AND ?';


        $regiones = Region::all();
        $departamentos = Departamento::all();
        $municipios = Municipio::all();
        
    
        $html = view('pdf.RP_PorFecha', compact('departamentos', 'municipios', 'regiones', 'queryRegion', 'queryDepa', 'queryMuni', 'fecha_inicio', 'fecha_fin') )->render();


        $mpdf = new Mpdf([
            'tempDir'=>storage_path('/app/public/tempdir'),
            // "format" => "A4",
             "format" => 'letter',
            //'format' => [215.9, 279.4],
            'margin_left' => 15,
            'margin_right' => 15,
            'margin_top' => 10,
            'margin_bottom' => 10,
            //'orientation' => 'L'
        ]);

        //$mpdf->SetTopMargin(7.5);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output('ReporteGeneralQuejas.pdf', "I");
    }
}
