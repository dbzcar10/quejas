<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bitacora;
use App\Models\Queja;
use App\Models\Region;
use App\Models\Departamento;
use App\Models\Municipio;
use App\Models\Documento;
use Storage;
use Carbon\Carbon;

class QuejasController extends Controller
{
    public function __construct(Bitacora $bitacora){
        $this->bitacora = $bitacora;
    }

    public function index()
    {
        $regiones = Region::all();
        $departamentos = Departamento::all();
        $municipios = Municipio::all();

        return view('admin.quejas.index', compact('regiones', 'departamentos', 'municipios'));
    }

    public function getJson(Request $request)
    {
        if(!$request->ajax()) return abort('403');

        //dd($request->fecha_inicio);

        $fecha_inicio = $request->fecha_inicio != '' ? new Carbon($request->fecha_inicio) : '' ;
        $fecha_fin =  $request->fecha_fin != '' ? new Carbon($request->fecha_fin) : '' ;

        if(!empty($request->fecha_inicio)){
            $quejas = Queja::with(['sucursal.comercio', 'sucursal.departamento.region', 'sucursal.departamento', 'sucursal.municipio'])->select('quejas.*')
            ->whereBetween('quejas.fecha_queja', array($fecha_inicio, $fecha_fin));
            //dd($quejas);
        }else{
            $quejas = Queja::with(['sucursal.comercio', 'sucursal.departamento.region', 'sucursal.departamento', 'sucursal.municipio'])->select('quejas.*');
        }

        return datatables()
        ->eloquent($quejas)
        //->eloquent(Queja::with(['sucursal.comercio', 'sucursal.departamento.region', 'sucursal.departamento', 'sucursal.municipio']) )
        ->editColumn('fecha_queja', function($request){
            return $request->fecha_queja->format('d-m-Y');
        })
        ->addColumn('btn', 'admin.quejas.acciones')
        ->rawColumns(['btn'])
        ->make(true);
    }

    public function show(Queja $queja)
    {
        //$queja->with('sucursal');
        return view('admin.quejas.show', compact('queja'));
    }

    public function documento(Queja $queja) 
    {
        $file = Storage::get($queja->documento->url);
        return  Response($file, 200, [
        'Content-Type' => 'application/pdf',
        //'Content-Disposition' => 'inline; filename="'.$queja->documentos[0]->nombre.'"'
        ]);
    }

}
