<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bitacora;
use App\Models\Comercio;
use App\Models\Sucursal;
use App\Models\Region;
use App\Models\Departamento;
use App\Models\Municipio;
use App\Models\Documento;
use Storage;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Validator;


class ComerciosController extends Controller
{
    public function __construct(Bitacora $bitacora){
        $this->bitacora = $bitacora;
    }

    public function getJson(Request $request)
    {
        if(!$request->ajax()) return abort('403');

        $comercios = DB::select('SELECT distinct C.*, COUNT(S.id) AS cantidad_sucursales, 
                                        (SELECT COUNT(Q.id) AS cantidad_quejas from quejas Q
                                        INNER JOIN sucursales S ON S.id = Q.sucursal_id
                                        INNER JOIN comercios C2 ON C2.id = S.comercio_id
                                        WHERE Q.deleted_at IS NULL
                                        AND C2.id = C.id) AS cantidad_quejas
                                FROM comercios C 
                                LEFT JOIN sucursales S ON C.id = S.comercio_id AND S.deleted_at IS NULL WHERE C.deleted_at IS NULL GROUP BY C.id');
        

        return datatables()
        ->of($comercios)
        /*->editColumn('sucursales', function($request){
            return $request->sucursales->count();
        })*/
        ->addColumn('btn', 'admin.comercios.acciones')
        ->rawColumns(['btn'])
        ->make(true);
    }

    public function index()
    {
        $regiones = Region::all();
        $departamentos = Departamento::all();
        $municipios = Municipio::all();

        return view('admin.comercios.index', compact('regiones', 'departamentos', 'municipios'));
    }

    public function create()
    {
        return view('admin.comercios.create');
    }

    public function store(Request $request)
    {
        $campos = $request->validate($this->reglas());

        $conteo = DB::select('SELECT COUNT(*) AS conteo
        FROM comercios C INNER JOIN sucursales S ON C.id = S.comercio_id
        WHERE S.municipio_id = ? AND S.direccion = ? AND C.nit = ? AND C.deleted_at IS NULL', [$campos['municipio'], $campos['direccion'], $campos['nit']]);

        if($conteo['0']->conteo > 0){
            return back()->withNotificacion(['mensaje' => 'Los datos ingresados ya se han registrado anteriormente', 'tipo' => 'warning', 'icon' => 'warning'])
                         ->withInput();
        };

        $comercio = new Comercio();
        $comercio->nit = $campos['nit'];
        $comercio->nombre_comercio = $campos['nombre_comercial'];
        $comercio->razon_social = $campos['razon_social'];
        $comercio->save();

        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $comercio->id, 'nombre_tabla' => 'comercios', 'accion' => 'agregar', 'info_anterior' => '', 'info_nueva' => $comercio]);

        $sucursal = new Sucursal();
        $sucursal->direccion = $campos['direccion'];
        $sucursal->telefono = $campos['telefono'];
        $sucursal->correo = $campos['correo'];
        $sucursal->departamento_id = $campos['departamento'];
        $sucursal->municipio_id = $campos['municipio'];
        $sucursal->comercio_id = $comercio->id;
        $sucursal->save();

        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $sucursal->id, 'nombre_tabla' => 'sucursales', 'accion' => 'agregar', 'info_anterior' => '', 'info_nueva' => $sucursal]);

        return redirect()->route('comercios')->withNotificacion(['mensaje' => 'El comercio ha sido agregado', 'tipo' => 'success', 'icon' => 'check']);
    }

    public function show(Comercio $comercio)
    {
        //$comercio->with('sucursal');
        return view('admin.comercios.show', compact('comercio'));
    }

    public function destroy(Comercio $comercio)
    {
        $comercioA = $comercio;
        if($comercio->has('sucursales')){
            foreach($comercio->sucursales as $sucursal){
                if($sucursal->has('quejas')){
                    foreach ($sucursal->quejas as $queja) {
                        $queja->delete();
                    }
                }
                $sucursal->delete();
            }
        }
        $comercio->delete();

        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $comercioA->id, 'nombre_tabla' => 'comercios', 'accion' => 'borrar', 'info_anterior' => $comercioA, 'info_nueva' => $comercio]);

        return response()->json(['data' => 'ok'], 200);
    }

    public function edit(Comercio $comercio)
    {
        return view('admin.comercios.edit', compact('comercio'));
    }

    public function update(Request $request, Comercio $comercio)
    {
        $campos = $request->validate($this->reglasUpdate());

        $comercioA = $comercio; 

        $comercio->nit = $campos['nit'];
        $comercio->nombre_comercio = $campos['nombre_comercial'];
        $comercio->razon_social = $campos['razon_social'];
        $comercio->save();

        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $comercio->id, 'nombre_tabla' => 'comercios', 'accion' => 'editar', 'info_anterior' => $comercioA, 'info_nueva' => $comercio]);

        return redirect()->route('comercios')->withNotificacion(['mensaje' => 'El comercio ha sido actualizado', 'tipo' => 'success', 'icon' => 'check']);
    }


    protected function reglas(){
        return  [
            'nit' => 'required|min:6|max:10',
            'razon_social' => 'nullable|max:100',
            'nombre_comercial' => 'required|max:100',
            'direccion' => 'required|max:255',
            'telefono' => ['nullable','max:8', 'regex:/([0-9]){8}/'],
            'correo' => 'nullable|email|max:100',
            'departamento' => 'required',
            'municipio' => 'required',
        ];
    }
    
    protected function reglasUpdate(){
        return  [
            'nit' => 'required|min:6|max:10',
            'razon_social' => 'nullable|max:100',
            'nombre_comercial' => 'required|max:100'
        ];
    }
}
