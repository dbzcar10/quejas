<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sucursal;
use App\Models\Comercio;
use App\Models\Bitacora;
use DB;

class SucursalesController extends Controller
{
    public function __construct(Bitacora $bitacora){
        $this->bitacora = $bitacora;
    }

    public function getJsonComercio(Request $request, Comercio $comercio)
    {
        if(!$request->ajax()) return abort('403');

        //$sucursales = Sucursal::where('comercio_id', $comercio->id)->with('departamento', 'municipio');
        $query = ('SELECT COUNT(Q.id) AS cantidad_quejas, S.comercio_id, S.id, S.direccion, S.telefono, S.correo, D.nombre AS nombre_departamento, M.nombre AS nombre_municipio FROM sucursales S 
        INNER JOIN municipios M ON S.municipio_id = M.id
        INNER JOIN departamentos D ON S.departamento_id = D.id 
        LEFT JOIN quejas Q ON Q.sucursal_id = S.id
        WHERE S.comercio_id = ? AND S.deleted_at IS NULL AND Q.deleted_at IS NULL GROUP BY S.id');

        $sucursales =  DB::select($query, [$comercio->id]);

        return datatables()
        ->of($sucursales)
        ->addColumn('btn', 'admin.sucursales.acciones')
        ->rawColumns(['btn'])
        ->make(true);
    }

    public function destroy(Sucursal $sucursal)
    {
        $sucursalA = $sucursal;
        if($sucursal->has('quejas')){
            foreach ($sucursal->quejas as $queja) {
                $queja->delete();
            }
        }
        $sucursal->delete();
        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $sucursalA->id, 'nombre_tabla' => 'sucursales', 'accion' => 'borrar', 'info_anterior' => $sucursalA, 'info_nueva' => $sucursal]);
        return response()->json(['data' => 'ok'], 200);
            
    }

    public function create(Comercio $comercio)
    {
        return view('admin.sucursales.create', compact('comercio'));
    }

    public function store(Request $request)
    {
        $campos = $request->validate($this->reglas());

        $conteo = DB::select('SELECT COUNT(*) AS conteo
        FROM comercios C INNER JOIN sucursales S ON C.id = S.comercio_id
        WHERE S.municipio_id = ? AND S.direccion = ? AND C.id = ? AND C.deleted_at IS NULL', [$campos['municipio'], $campos['direccion'], $request->comercio_id ]);

        if($conteo['0']->conteo > 0){
            return back()->withNotificacion(['mensaje' => 'Los datos ingresados ya se han registrado anteriormente', 'tipo' => 'warning', 'icon' => 'warning'])
                         ->withInput();
        };

        $sucursal = new Sucursal();
        $sucursal->direccion = $campos['direccion'];
        $sucursal->telefono = $campos['telefono'];
        $sucursal->correo = $campos['correo'];
        $sucursal->departamento_id = $campos['departamento'];
        $sucursal->municipio_id = $campos['municipio'];
        $sucursal->comercio_id = $request->comercio_id;
        $sucursal->save();

        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $sucursal->id, 'nombre_tabla' => 'sucursales', 'accion' => 'agregar', 'info_anterior' => '', 'info_nueva' => $sucursal]);

        return redirect()->route('comercios.show', $request->comercio_id)->withNotificacion(['mensaje' => 'La sucursal ha sido agregada', 'tipo' => 'success', 'icon' => 'check']);
    }

    public function edit(Sucursal $sucursal, Comercio $comercio)
    {
        return view('admin.sucursales.edit', compact('sucursal','comercio'));
    }

    public function update(Request $request, Sucursal $sucursal)
    {
        $campos = $request->validate($this->reglas());

        $conteo = DB::select('SELECT COUNT(*) AS conteo
        FROM comercios C INNER JOIN sucursales S ON C.id = S.comercio_id
        WHERE S.municipio_id = ? AND S.direccion = ? AND C.id = ? AND C.deleted_at IS NULL AND S.id != ?', [$campos['municipio'], $campos['direccion'], $request->comercio_id, $sucursal->id ]);

        if($conteo['0']->conteo > 0){
            return back()->withNotificacion(['mensaje' => 'Los datos ingresados ya se han registrado anteriormente', 'tipo' => 'warning', 'icon' => 'warning'])
                         ->withInput();
        };

        $sucursalA = $sucursal; 

        $sucursal->direccion = $campos['direccion'];
        $sucursal->telefono = $campos['telefono'];
        $sucursal->correo = $campos['correo'];
        $sucursal->departamento_id = $campos['departamento'];
        $sucursal->municipio_id = $campos['municipio'];
        $sucursal->save();


        //Bitácora
        $this->bitacora->createBitacora(['modelo_id' => $sucursal->id, 'nombre_tabla' => 'sucursales', 'accion' => 'editar', 'info_anterior' => $sucursalA, 'info_nueva' => $sucursal]);

        return redirect()->route('comercios.show', $request->comercio_id)->withNotificacion(['mensaje' => 'La sucursal ha sido actualizado', 'tipo' => 'success', 'icon' => 'check']);
    }

    protected function reglas(){
        return  [
            'direccion' => 'required|max:255',
            'telefono' => ['nullable','max:8', 'regex:/([0-9]){8}/'],
            'correo' => 'nullable|email|max:100',
            'departamento' => 'required',
            'municipio' => 'required',
        ];
    } 
}
